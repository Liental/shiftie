import { Time } from '../app/modules/Time'
import { Filter } from '../app/modules/Filter'
import { Subtitle } from '../app/modules/Subtitle'

describe ('Filter', () => {
  const filter = new Filter()

  beforeEach(() => {
    filter.id = undefined
    filter.startTime = undefined
    filter.endTime = undefined
    filter.content = undefined
    filter.orderBy = 1
    filter.sortClosure = (s: Subtitle) => s.id
  })

  it ('#clone | should return the exact copy of this object', () => {
    filter.id = 5
    filter.content = 'Hello'
    filter.startTime = new Time('00:11:01,2018')
    filter.endTime = new Time('00:20:18,1101')

    const clone = filter.clone()

    expect(clone.id).toBe(filter.id)
    expect(clone.content).toBe(filter.content)
    expect(clone.endTime.equals(filter.endTime)).toBeTruthy()
    expect(clone.startTime.equals(filter.startTime)).toBeTruthy()
  })

  it ('#isEmpty | should check if the filter is empty and return true', () => {
    expect(filter.isEmpty()).toBeTruthy()
  })
  
  it ('#isEmpty | should check if the filter is empty and return false', () => {
    filter.content = 'Hello'
    
    expect(filter.isEmpty()).toBeFalsy()
  })

  it ('#equals | should comapre two filters and return true', () => {
    filter.id = 5
    filter.content = 'Hello'
    
    expect(filter.equals(filter.clone())).toBeTruthy()    
  })

  it ('#equals | should comapre two filters and return false', () => {
    const compareFilter = new Filter()

    filter.id = 5
    filter.content = 'Hello'
    
    expect(filter.equals(compareFilter)).toBeFalsy()    
  })

  it ('#reset | should correctly reset a filter object', () => {
    const compareFilter = new Filter()

    filter.id = 5
    filter.content = 'Hello'
    filter.reset()

    expect(filter.equals(compareFilter)).toBeTruthy()
  })

  it ('#sort | should sort subtitles descending by ID', () => {
    filter.orderBy = -1

    const subtitles = [
      new Subtitle('1\r\n00:00:01,000 --> 00:00:02,00\r\nHello'),
      new Subtitle('2\r\n00:00:02,000 --> 00:00:03,00\r\nHow are you')
    ]

    const result = filter.sort(subtitles)

    expect(result[0].id).toBe(2)
  })

  it ('#checkTime | should check if the time has 0 hours and 15 milliseconds and return true', () => {
    const aTime = new Time('00:00:15,015')
    const bTime = new Time('-1:00:-1,015')

    expect(filter.checkTime(aTime, bTime)).toBeTruthy()
  })
  
  it ('#checkTime | should check if the time has 0 hours and 15 milliseconds and return false', () => {
    const aTime = new Time('00:01:15,015')
    const bTime = new Time('-1:00:-1,015')

    expect(filter.checkTime(aTime, bTime)).toBeFalsy()
  })

  it ('#filter | should filter the subtitles and return only these with "Mark" in content', () => {
    const subtitles = [
      new Subtitle('1\r\n00:00:01,000 --> 00:00:02,00\r\nHello Mark'),
      new Subtitle('2\r\n00:00:02,000 --> 00:00:03,00\r\nHow are you?'),
      new Subtitle('3\r\n00:00:04,000 --> 00:00:05,00\r\nMark your way, Mark?')
    ]

    filter.content = 'Mark'
    const result = filter.filter(subtitles)
    
    expect(result.length).toBe(2)
    expect(result[0].equals(subtitles[0])).toBeTruthy()
    expect(result[1].equals(subtitles[2])).toBeTruthy()
  })
})