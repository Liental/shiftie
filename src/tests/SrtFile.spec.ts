import { SrtFile } from '../app/modules/SrtFile'
import { Subtitle } from 'src/app/modules/Subtitle';

const text = '1\r\n00:00:00,500 --> 00:00:01,250\r\nHello darkness, my old friend!\r\n\r\n2\r\n00:00:02,000 --> 00:00:03,500\r\nIve come to talk to you again\r\n\r\n'

describe ('SrtFile', () => {
  it ('#constructor | should initialise SrtFile object with given text', () => {
    const result = new SrtFile(text)

    const firstSubtitle = new Subtitle('1\r\n00:00:00,500 --> 00:00:01,250\r\nHello darkness, my old friend!')
    const secondSubtitle = new Subtitle('2\r\n00:00:02,000 --> 00:00:03,500\r\nIve come to talk to you again')

    expect(result.subtitles.length).toBe(2)
    expect(result.subtitles[0].equals(firstSubtitle)).toBeTruthy()
    expect(result.subtitles[1].equals(secondSubtitle)).toBeTruthy()
  })
  
  it ('#findSubtitle | should return 1', () => {
    const srtFile = new SrtFile(text)
    const subtitle = srtFile.subtitles[1]

    expect(srtFile.findSubtitle(subtitle)).toBe(1)
  })

  it ('#findSubtitle | should return -1', () => {
    const srtFile = new SrtFile(text)
    const subtitle = new Subtitle('1\r\n00:00:01,000 --> 00:00:03,500\r\nHello lightness')

    expect(srtFile.findSubtitle(subtitle)).toBe(-1)
  })

  it ('#findSubtitleById | should return 1', () => {
    const srtFile = new SrtFile(text)
    const subtitle = srtFile.subtitles[1]

    expect(srtFile.findSubtitleById(subtitle)).toBe(1)
  })

  it ('#findSubtitleById | should return -1', () => {
    const srtFile = new SrtFile(text)
    const id = 'im-not-existant-id'

    expect(srtFile.findSubtitleById(id)).toBe(-1)
  })

  it ('#cloneSubtitles | should return exact copy of all the subtitles', () => {
    const srtFile = new SrtFile(text)
    const result = srtFile.cloneSubtitles()

    expect(result[0].equals(srtFile.subtitles[0])).toBeTruthy()
    expect(result[1].equals(srtFile.subtitles[1])).toBeTruthy()
  })

  it ('#displayRaw | should pretty print the SRT object', () => {
    const srtFile = new SrtFile(text)

    expect(srtFile.displayRaw()).toBe(text)
  })

  it('#displayHTML | should pretty print the SRT object in HTML', () => {
    const srtFile = new SrtFile(text)
    const localText = text.split('\r\n').join('<br>')

    expect(srtFile.displayHTML()).toBe(localText)
  })
})
