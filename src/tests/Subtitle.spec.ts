import { Subtitle } from '../app/modules/Subtitle'

const text = '1\r\n00:00:00,500 --> 00:00:01,250\r\nHello darkness, my old friend!\r\nIve come to talk to you again'

describe ('Subtitle', () => {
  it ('#constructor | should initialise Subtitle object with text', () => {
    const subtitle = new Subtitle(text)

    expect(subtitle.id).toBe(1)
    expect(subtitle.content).toBe('Hello darkness, my old friend!\r\nIve come to talk to you again')
    expect(subtitle.startTime.display()).toBe('00:00:00,500')
    expect(subtitle.endTime.display()).toBe('00:00:01,250')
  })

  it ('#clone | should clone given subtitle', () => {
    const subtitle = new Subtitle(text)
    const result = subtitle.clone()

    expect(result.id).toBe(subtitle.id)
    expect(result.content).toBe(subtitle.content)
    expect(result.internalID).toBe(subtitle.internalID)
    expect(result.startTime.display()).toBe(subtitle.startTime.display())
    expect(result.endTime.display()).toBe(subtitle.endTime.display())
  })
  
  it ('#shift | should shift time forwards by 01:02:03,004', () => {
    const subtitle = new Subtitle(text)
    
    subtitle.shift('01:02:03,004')
    
    expect(subtitle.startTime.display()).toBe('01:02:03,504')
    expect(subtitle.endTime.display()).toBe('01:02:04,254')
  })

  it ('#shift | should shift time backwards by 01:02:03,004', () => {
    const localText = text.replace('00:00:00,500 --> 00:00:01,250', '01:02:03,504 --> 01:02:04,254')
    const subtitle = new Subtitle(localText)

    subtitle.shift('01:02:03,004', { backwards: true, shiftStart: true, shiftEnd: true })
    
    expect(subtitle.startTime.display()).toBe('00:00:00,500')
    expect(subtitle.endTime.display()).toBe('00:00:01,250')
  })

  it ('#equals | should return false', () => {
    const localText = text.replace('Hello darkness', 'Goodbye sparkness')
    
    const subtitle = new Subtitle(text)
    const compareSubtitle = new Subtitle(localText)
    const result = subtitle.equals(compareSubtitle)

    expect(result).toBeFalsy()
  })

  it ('#equals | should return true', () => {
    const subtitle = new Subtitle(text)
    const result = subtitle.equals(subtitle)

    expect(result).toBe(true)
  })

  it ('#getDuration | should return 750', () => {
    const subtitle = new Subtitle(text)
    const result = subtitle.getDuration()

    expect(result).toBe(750)
  })

  it ('#displayRaw | should return pretty print in SRT format', () => {
    const subtitle = new Subtitle(text)
    const result = subtitle.displayRaw()

    expect(result).toBe(text)
  })

  it ('#displayHTML | should return pretty print in HTML', () => {
    const localText = text.replace('\r\n', '<br>').replace('\r\n', '<br>')
    const subtitle = new Subtitle(text)
    const result = subtitle.displayHTML()

    expect(result).toBe(localText)
  })
})