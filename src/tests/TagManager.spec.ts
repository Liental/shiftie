import { TagParameter, TagManager, TagSettings, TagEnum } from 'src/app/classes/TagManager'
import { Subtitle } from 'src/app/modules/Subtitle';

describe ('TagManager', () => {
  it ('#getParamsDisplay | should return pretty print of color and family', () => {
    const parameters: TagParameter[] = [ { name: 'color', value: '#ffffff' }, { name: 'family', value: 'Roboto' } ]
    const display = TagManager.getParamsDisplay(parameters)

    expect(display).toBe(' color="#ffffff" family="Roboto"')
  })

  it ('#containsTag | should check if the string is italic and return true', () => {
    const content = '<i>Hello there good sir!</i>'
    const settings: TagSettings = { tag: TagEnum.ITALIC }
    const result = TagManager.containsTag(content, settings)

    expect(result).toBeTruthy()
  })
  
  it ('#containsTag | should check if the string is italic and return false', () => {
    const content = '<b>Hello there good sir!</b>'
    const settings: TagSettings = { tag: TagEnum.ITALIC }
    const result = TagManager.containsTag(content, settings)

    expect(result).toBeFalsy()
  })

  it ('#changeTag | should realise that the subtitle doesnt contain italic and add it', () => {
    const subtitle = new Subtitle('1\r\n00:00:00,000 --> 00:00:00,000\r\n<b>Hello there good sir!</b>')
    const settings: TagSettings = { tag: TagEnum.ITALIC }
    
    TagManager.changeTag([ subtitle ], settings)

    expect(subtitle.content).toBe('<i><b>Hello there good sir!</b></i>')
  })

  it ('#changeTag | should realise that the subtitle contains italic and remove it', () => {
    const subtitle = new Subtitle('1\r\n00:00:00,000 --> 00:00:00,000\r\n<i>Hello there good sir!</i>')
    const settings: TagSettings = { tag: TagEnum.ITALIC }
    
    TagManager.changeTag([ subtitle ], settings)
    expect(subtitle.content).toBe('Hello there good sir!')
  })

  it ('#addTag | should add a font tag with #ff00ff to the first word of given content', () => {
    const content = 'Hello there!'
    const settings: TagSettings = { 
      tag: TagEnum.FONT, 
      selection: { start: 0, end: 5 }, 
      parameters: [ { name: 'color', value: '#ff00ff'} ] 
    }

    const result = TagManager.addTags(content, settings)
    expect(result).toBe('<font color="#ff00ff">Hello</font> there!')
  })

  it ('#addTag | should add an italic tag to the given content', () => {
    const content = 'Hello there!'
    const settings: TagSettings = { tag: TagEnum.ITALIC }

    const result = TagManager.addTags(content, settings)
    expect(result).toBe('<i>Hello there!</i>')
  })

  it ('#removeTag | should remove a font tag from the first word of given content', () => {
    const content = '<font color="#ff00ff">Hello</font> there!'
    const settings: TagSettings = { 
      tag: TagEnum.FONT, 
      selection: { start: 0, end: 34 }, 
      parameters: [ { name: 'color', value: '#ff00ff'} ] 
    }

    const result = TagManager.removeTags(content, settings)
    expect(result).toBe('Hello there!')
  })
})