import { Notifier } from '../app/modules/Notifier' 
import { Notification, NotificationType } from '../app/classes/Notification'

describe ('Notifier', () => {
  const notifier = new Notifier()

  beforeEach(() => {
    notifier.limit = 5
    notifier.notifications = [ ]
    notifier.queue = [ ]
  })

  it ('#addNotification | should add given notification and not set up a timeout', () => {
    const notification = new Notification('Hello there', NotificationType.INPUT, NaN)
    const timeoutID = notifier.addNotification(notification)

    expect(timeoutID).toBeUndefined()
    expect(notifier.notifications[0]).toBe(notification)
  })

  it ('#addNotification | should add given notification and set up timeout', () => {
    const notification = new Notification('Hello there', NotificationType.INFO, 2500)
    const timeoutID = notifier.addNotification(notification)

    expect(timeoutID).toBeDefined()
    expect(notifier.notifications[0]).toBe(notification)
  })

  it ('#addNotification | should add given notification to the queue', () => {
    const notification = new Notification('Hello there', NotificationType.INFO, 2500)
    
    notifier.notifications = [ notification, notification, notification, notification, notification ]

    const timeoutID = notifier.addNotification(notification)

    expect(timeoutID).toBeUndefined()
    expect(notifier.queue[0]).toBe(notification)
  })

  it ('#closeNotification | should remove given notification from the table', () => {
    const notification = new Notification('Hello there', NotificationType.INFO, 2500)
    
    notifier.addNotification(notification)
    notifier.closeNotification(notification)
    
    expect(notifier.notifications.length).toBe(0)
  })

  it ('#closeNotification | should remove given notification from the table and put one from queue in their position', () => {
    const notification = new Notification('Hello there', NotificationType.INFO, 2500)
    const anotherNotification = new Notification('Hello there again')

    notifier.queue = [ anotherNotification ]
    notifier.addNotification(notification)
    notifier.closeNotification(notification)
    
    expect(notifier.notifications.length).toBe(1)
    expect(notifier.notifications[0]).toBe(anotherNotification)
  })
})