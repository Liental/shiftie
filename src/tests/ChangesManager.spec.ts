import { Subtitle } from '../app/modules/Subtitle'
import { ChangesManager } from '../app/modules/ChangesManager'
import { SubtitleChange } from '../app/classes/SubtitleChange'

describe ('ChangesManager', () => {
  it ('#reset | should reset the manager to its initial state', () => {
    const manager = new ChangesManager()
    manager.madeChanges = [ [ new SubtitleChange(undefined, undefined) ] ]
    manager.revertedChanges = [ [ new SubtitleChange(undefined, undefined) ] ]
  
    manager.reset()

    expect(manager.madeChanges.length).toBe(0)
    expect(manager.revertedChanges.length).toBe(0)
  })

  it ('#compareChanges | should compare given changes and return true', () => {
    const manager = new ChangesManager()
    const subtitle = new Subtitle('1\r\n00:00:01,000 --> 00:00:02,000\r\nHello darkness')
    
    manager.madeChanges = [ [ new SubtitleChange(subtitle, undefined), new SubtitleChange(undefined, subtitle) ] ]
    const result = manager.compareChanges(manager.madeChanges[0])

    expect(result).toBeTruthy()
  })

  it ('#compareChanges | should compare given changes and return false', () => {
    const manager = new ChangesManager()
    const subtitle = new Subtitle('1\r\n00:00:01,000 --> 00:00:02,000\r\nHello darkness')
    
    manager.madeChanges = [ [ new SubtitleChange(subtitle, undefined), new SubtitleChange(undefined, subtitle) ] ]
    const result = manager.compareChanges([ manager.madeChanges[0][0] ])

    expect(result).toBeFalsy()
  })

  it ('#addChanges | should empty revertedChanges array add some changes to madeChanges', () => {
    const manager = new ChangesManager()
    const subtitle = new Subtitle('1\r\n00:00:01,000 --> 00:00:02,000\r\nHello darkness')
    const changes = [ new SubtitleChange(subtitle, undefined), new SubtitleChange(undefined, subtitle) ]

    manager.revertedChanges = [ changes ]
    manager.addChanges(changes)

    expect(manager.madeChanges.length).toBe(1)    
    expect(manager.revertedChanges.length).toBe(0)
  })

  it ('#popChanges | should remove and return the lastest change of madeChanges', () => {
    const manager = new ChangesManager()
    const subtitle = new Subtitle('1\r\n00:00:01,000 --> 00:00:02,000\r\nHello darkness')
    const secondSubtitle = new Subtitle('2\r\n00:00:02,000 --> 00:00:03,000\r\nHi darkness')
    const changes = [ new SubtitleChange(subtitle, secondSubtitle), new SubtitleChange(secondSubtitle, subtitle) ]

    manager.madeChanges = [ changes ]
    const result = manager.popChanges()

    expect(manager.madeChanges.length).toBe(0)
    expect(result[0].subtitleFrom.equals(changes[0].subtitleFrom)).toBeTruthy()
    expect(result[0].subtitleTo.equals(changes[0].subtitleTo)).toBeTruthy()

    expect(result[1].subtitleFrom.equals(changes[1].subtitleFrom)).toBeTruthy()
    expect(result[1].subtitleTo.equals(changes[1].subtitleTo)).toBeTruthy()
  })
})