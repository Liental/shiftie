import { SelectionService } from 'src/app/services/SelectionService.service'
import { ApplicationService } from 'src/app/services/ApplicationService.service'
import { FilterService } from 'src/app/services/FilterService.service'
import { NotifierService } from 'src/app/services/NotifierService.service'
import { ChangesService } from 'src/app/services/ChangesService.service'
import { Subtitle } from 'src/app/modules/Subtitle'

describe ('SelectionService', () => {
  let application = new ApplicationService()
  let filter = new FilterService()
  let notifier = new NotifierService()
  let changes = new ChangesService(application, notifier)
  let selection = new SelectionService(application, filter, notifier, changes)

  beforeEach (() => {
    filter = new FilterService()
    notifier = new NotifierService()
    application = new ApplicationService()
    changes = new ChangesService(application, notifier)
    selection = new SelectionService(application, filter, notifier, changes)

    application.newSrtFile.subtitles = [
      new Subtitle('1\r\n00:00:00,000 --> 00:00:00,000\r\nHello there my sir'),
      new Subtitle('2\r\n00:00:00,000 --> 00:00:00,000\r\nHow are you sir'),
      new Subtitle('3\r\n00:00:00,000 --> 00:00:00,000\r\nI hope you are fine over there!')
    ]
  })

  it ('#getTemporary | should return the temporarly selected subtitle', () => {
    const subtitle = application.newSrtFile.subtitles[0]
    selection.manager.temporarySelect = subtitle
    
    const result = selection.getTemporary()
    expect(result).toBe(subtitle)
  })

  it ('#setTemporary | should set the temporary subtitle to given one', () => {
    const subtitle = application.newSrtFile.subtitles[0]
    
    selection.setTemporary(subtitle)
    expect(selection.manager.temporarySelect).toBe(subtitle)
  })

  it ('#selectAll | should select all subtitles', () => {
    selection.selectAll()

    const selected = selection.manager.selected
    const subtitles = application.newSrtFile.subtitles
    
    expect(selected[0]).toBe(subtitles[0])
    expect(selected[1]).toBe(subtitles[1])
    expect(selected[2]).toBe(subtitles[2])
  })

  it ('#selectAll | should select filtered subtitles', () => {
    filter.manager.content = 'sir'
    selection.selectAll()
    
    const selected = selection.manager.selected
    const subtitles = application.newSrtFile.subtitles

    expect(selected.length).toBe(2)
    expect(selected[0]).toBe(subtitles[0])
    expect(selected[1]).toBe(subtitles[1])
  })

  it ('#chooseSubtitle | should set given subtitle to be the only one selected', () => {
    const subtitles = application.newSrtFile.subtitles
    selection.manager.selected = subtitles

    selection.chooseSubtitle(subtitles[0])
    expect(application.settings.isEditing).toBeTruthy()
    expect(selection.manager.selected.length).toBe(1)
    expect(selection.manager.selected[0]).toBe(subtitles[0])
  })

  it ('#chooseSubtitle | should add given subtitle to selection', () => {
    const subtitles = application.newSrtFile.subtitles
    
    application.settings.isSelecting = true
    selection.manager.selected = [ subtitles[0] ]

    selection.chooseSubtitle(subtitles[1])
    expect(application.settings.isEditing).toBeFalsy()
    expect(selection.manager.selected.length).toBe(2)
    expect(selection.manager.selected[0]).toBe(subtitles[0])
    expect(selection.manager.selected[1]).toBe(subtitles[1])
  })

  it ('#chooseSubtitle | should add first and second subtitle to the selection', () => {
    const subtitles = application.newSrtFile.subtitles
    
    application.settings.isFromTo = true
    selection.manager.lastSelected = subtitles[0]
    
    selection.chooseSubtitle(subtitles[1])
    expect(application.settings.isEditing).toBeFalsy()
    expect(selection.manager.selected.length).toBe(2)
    expect(selection.manager.selected[0]).toBe(subtitles[0])
    expect(selection.manager.selected[1]).toBe(subtitles[1])
  })

  it ('#selectFromTo | should selected first and second subtitle', () => {
    const subtitles = application.newSrtFile.subtitles
    selection.manager.lastSelected = subtitles[0]

    selection.selectFromTo(subtitles[1])
    expect(selection.manager.selected.length).toBe(2)
    expect(selection.manager.selected[0]).toBe(subtitles[0])
    expect(selection.manager.selected[1]).toBe(subtitles[1])
  })

  it ('#selectFromTo | should selected first and second subtitle', () => {
    const subtitles = application.newSrtFile.subtitles
    selection.manager.lastSelected = subtitles[0]
    filter.manager.content = 'there'

    selection.selectFromTo(subtitles[2])
    expect(selection.manager.selected.length).toBe(2)
    expect(selection.manager.selected[0]).toBe(subtitles[0])
    expect(selection.manager.selected[1]).toBe(subtitles[2])
  })

  it ('#removeSelected | should remove selected subtitles from the file', () => {
    const subtitles = application.newSrtFile.subtitles
    selection.manager.selected = [ subtitles[1], subtitles[2] ]
    
    selection.removeSelected()
    expect(application.newSrtFile.subtitles.length).toBe(1)
    expect(application.newSrtFile.subtitles[0]).toBe(subtitles[0])
  })
})