import { ClickChecker } from '../app/services/ClickChecker.service'
import { DropdownComponent } from '../app/components/application/dropdowns/Dropdown/Dropdown.component'

describe ('ClickChecker', () => {
  const checker = new ClickChecker()

  it ('#checkElements | should check if the click matches given element and return true', () => {
    const element = document.createElement('span')
    element.innerHTML = 'Hello there!'

    expect(checker.checkElement(element, element)).toBeTruthy()
  })
  
  it ('#checkElements | should check if the click matches given element and return false', () => {
    const element = document.createElement('span')
    const elementToMatch = document.createElement('span')
  
    element.innerHTML = 'Hello there!'
    elementToMatch.innerHTML = 'Bye there!'

    expect(checker.checkElement(element, elementToMatch)).toBeFalsy()
  })

  it ('#checkArray | should check if given list matches the click and set revealed to false', () => {
    const component = new DropdownComponent(undefined)
    const element = document.createElement('span')
    const elementToMatch = document.createElement('span')
    
    element.innerHTML = 'Hello there!'
    elementToMatch.innerHTML = 'Bye there!'
    component.dropdown = element
    component.revealed = true

    checker.checkArray([ component ], 'dropdown', elementToMatch)

    expect(component.revealed).toBeFalsy()
  })
  
  it ('#checkArray | should check if given list matches the click and leave the component intact', () => {
    const component = new DropdownComponent(undefined)
    const element = document.createElement('span')
    const elementToMatch = document.createElement('span')
    
    element.innerHTML = 'Hello there!'
    elementToMatch.innerHTML = 'Bye there!'
    component.dropdown = element

    checker.checkArray([ component ], 'dropdown', elementToMatch)

    expect(component.revealed).toBeFalsy()
  })
})