import { Time } from '../app/modules/Time'

describe ('Time', () => {
  it ('#constructor | should initialise Time object with a string', () => {
    const time = new Time('03:03:32,018')
    
    expect(time.hours).toBe(3)
    expect(time.minutes).toBe(3)
    expect(time.seconds).toBe(32)
    expect(time.milliseconds).toBe(18)
  })

  it ('#constructor | should initialise Time object with a number', () => {
    const time = new Time(11012018)
 
    expect(time.hours).toBe(3)
    expect(time.minutes).toBe(3)
    expect(time.seconds).toBe(32)
    expect(time.milliseconds).toBe(18) 
  })
  
  it ('#shift | should shift the object shiftied by 11012018 milliseconds', () => {
    const time = new Time(11012018)
    
    time.shift(11012018)

    expect(time.hours).toBe(6)
    expect(time.minutes).toBe(7)
    expect(time.seconds).toBe(4)
    expect(time.milliseconds).toBe(36) 
  })

  it ('#shift | should shift the object shiftied backwards by 11012018 milliseconds', () => {
    const time = new Time(11012018)
    
    time.shift(11012018, true)

    expect(time.hours).toBe(0)
    expect(time.minutes).toBe(0)
    expect(time.seconds).toBe(0)
    expect(time.milliseconds).toBe(0) 
  })

  it ('#clone | should return a clone of given object', () => {
    const time = new Time(11012018)
    const clone = time.clone()

    expect(clone.hours).toBe(time.hours)
    expect(clone.minutes).toBe(time.minutes)
    expect(clone.seconds).toBe(time.seconds)
    expect(clone.milliseconds).toBe(time.milliseconds)
  })

  it ('#display | should return 03:03:32:018', () => {
    const time = new Time(11012018)
    const result = time.display()

    expect(result).toBe('03:03:32,018')
  })

  it ('#equals | should return true', () => {
    const time = new Time(11012018)
    const compareTime = new Time(11012018)
    const result = time.equals(compareTime)

    expect(result).toBeTruthy()
  })

  it ('#equals | should return false', () => {
    const time = new Time(11012018)
    const compareTime = new Time(22024036)
    const result = time.equals(compareTime)

    expect(result).toBe(false)
  })

  it ('#getMilliseconds | should return 11012018 milliseconds', () => {
    const time = new Time(11012018)
    const result = time.getMilliseconds()
    
    expect(result).toBe(11012018)
  })
})
