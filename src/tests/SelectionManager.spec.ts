import { SelectionManager } from '../app/modules/SelectionManager'
import { Subtitle } from 'src/app/modules/Subtitle'
import { SrtFile } from 'src/app/modules/SrtFile';

describe ('SelectionManager', () => {
  const manager = new SelectionManager()

  let subtitles = [ 
    new Subtitle('1\r\n00:00:00,000 --> 00:00:00,000\r\n Hello'),
    new Subtitle('2\r\n00:00:01,000 --> 00:00:01,000\r\n Hey'),
    new Subtitle('3\r\n00:00:02,000 --> 00:00:02,000\r\n Heylo')
  ]

  beforeEach(() => {
    manager.selected = []
    manager.lastSelected = undefined
    manager.temporarySelect = undefined
    
    subtitles = [ 
      new Subtitle('1\r\n00:00:00,000 --> 00:00:00,000\r\n Hello'),
      new Subtitle('2\r\n00:00:01,000 --> 00:00:01,000\r\n Hey'),
      new Subtitle('3\r\n00:00:02,000 --> 00:00:02,000\r\n Heylo')
    ]
  })

  it ('#reset | should reset this object to its initial form', () => {
    manager.lastSelected = subtitles[0]
    manager.temporarySelect = subtitles[1]
    manager.selected = subtitles

    manager.reset()

    expect(manager.lastSelected).toBeUndefined()    
    expect(manager.temporarySelect).toBeUndefined()
    expect(manager.selected.length).toBe(0)    
  })

  it ('#deleteFirst | should delete the first element of the selected subtitles from the list', () => {
    manager.selected = subtitles
    manager.deleteFirst()

    expect(manager.selected.length).toBe(2)
    expect(manager.selected[0]).toBe(subtitles[1])
  })

  it ('#selectSubtitle | should clear the selection list and add only given subtitle', () => {
    manager.selected = subtitles
    manager.selectSubtitle(subtitles[2])

    expect(manager.selected.length).toBe(1)
    expect(manager.selected[0]).toBe(subtitles[2])
  })

  it ('#selectSubtitle | should deny selecting given subtitle', () => {
    manager.selected = subtitles
    manager.selectSubtitle(undefined)

    expect(manager.selected.length).toBe(3)
    expect(manager.selected[0]).toBe(subtitles[0])
    expect(manager.selected[1]).toBe(subtitles[1])
    expect(manager.selected[2]).toBe(subtitles[2])
  })

  it ('#selectSubtitles | should select all the given subtitles', () => {
    const srtFile = new SrtFile()

    srtFile.subtitles = subtitles 
    manager.selectSubtitles(srtFile.subtitles)
    
    expect(manager.selected.length).toBe(3)
    expect(manager.selected[0]).toBe(subtitles[0])
    expect(manager.selected[1]).toBe(subtitles[1])
    expect(manager.selected[2]).toBe(subtitles[2])
  })

  it ('#selectFromTo | should select subtitles from the first to the third', () => {
    const subtitle = new Subtitle('4\r\n00:00:03,000 --> 00:00:03,000\r\nSalut!')
    const newSubtitles = subtitles.concat([ subtitle ])

    manager.selectFromTo(newSubtitles, newSubtitles[0], newSubtitles[2])

    expect(manager.selected.length).toBe(3)
    expect(manager.selected[0]).toBe(newSubtitles[0])
    expect(manager.selected[1]).toBe(newSubtitles[1])
    expect(manager.selected[2]).toBe(newSubtitles[2])
  })

  it ('#findSubtitle | should find the index of given subtitle and return it', () => {
    manager.selected = subtitles
    expect(manager.findSubtitle(subtitles[1])).toBe(1)
  })

  
  it ('#findSubtitle | should fail on finding the index of given subtitle and return -1', () => {
    manager.selected = subtitles
    expect(manager.findSubtitle(undefined)).toBe(-1)
  })

  it ('#isSelected | should find given subtitle and return true', () => {
    manager.selected = subtitles
    expect(manager.isSelected(subtitles[0])).toBeTruthy()
  })
  
  it ('#isSelected | should fail finding given subtitle and return false', () => {
    manager.selected = [ subtitles[1] ]
    expect(manager.isSelected(subtitles[0])).toBeFalsy()
  })

  it ('#chooseTemporary | should add the temporary subtitle to selection', () => {
    const subtitle = new Subtitle('4\r\n00:00:04,000 --> 00:00:04,000\r\nSalut')

    manager.selected = subtitles
    manager.temporarySelect = subtitle
    manager.chooseTemporary(true)

    expect(manager.selected.length).toBe(4)
    expect(manager.selected[0]).toBe(subtitles[0])
    expect(manager.selected[1]).toBe(subtitles[1])
    expect(manager.selected[2]).toBe(subtitles[2])
    expect(manager.selected[3]).toBe(subtitle)
  })

  it ('#chooseTemporary | should clear the selection and select the temporary subtitle', () => {
    const subtitle = new Subtitle('4\r\n00:00:04,000 --> 00:00:04,000\r\nSalut')

    manager.selected = subtitles
    manager.temporarySelect = subtitle
    manager.chooseTemporary()

    expect(manager.selected.length).toBe(1)
    expect(manager.selected[0]).toBe(subtitle)
  })

  it ('#changeSelection | shouldnt find given subtitle in selection and then select it', () => {
    manager.changeSelection(subtitles[0])

    expect(manager.selected.length).toBe(1)
    expect(manager.selected[0]).toBe(subtitles[0])
  })

  it ('#changeSelection | should find given subtitle in selection and deselect it', () => {
    const cloned = Array.from(subtitles)
    
    manager.selected = subtitles
    manager.changeSelection(subtitles[0])

    expect(manager.selected.length).toBe(2)
    expect(manager.selected[0]).toBe(cloned[1])
    expect(manager.selected[1]).toBe(cloned[2])
  })

  it ('#removeSelected | should remove all selected subtitles from given SRT files', () => {
    const srt = new SrtFile()
    
    srt.subtitles = Array.from(subtitles)
    manager.selected = [ subtitles[0], subtitles[1] ]
    manager.removeSelected(srt)

    expect(srt.subtitles.length).toBe(1)
    expect(srt.subtitles[0]).toBe(subtitles[2])
    expect(manager.selected.length).toBe(0)
    expect(manager.temporarySelect).toBe(undefined)
  })
})