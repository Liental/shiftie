import { IDGenerator } from '../app/classes/IDGenerator'

describe ('IDGenerator', () => {
    it ('#generateAlphabet | should generate an alphabet with all cases', () => {
        const result = IDGenerator.generateAlphabet()
        expect(result).toBe('ABCDEFGHIJKLMNOPQRSTUWVXYZabcdefghijklmnopqrstuwvxyz0123456789')
    })

    it ('#generateID | should generate an ID with 5 parts of 5 characters', () => {
        const result = IDGenerator.generateAlphabet()
        expect(result).toMatch(/(([a-z]|[A-Z]|[0-9]){5}-?){5}/)
    })
})