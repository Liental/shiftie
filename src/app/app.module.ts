import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { Routes, RouterModule } from '@angular/router'
import { BrowserModule } from '@angular/platform-browser'

import { HomePage } from './pages/Home/Home.component'
import { AboutPage } from './pages/About/About.component'

import { ClickChecker } from './services/ClickChecker.service'
import { NotifierService } from './services/NotifierService.service'
import { ChangesService } from './services/ChangesService.service'

import { AppComponent } from './root/app.component'
import { LightSwitchComponent } from './components/static/LightSwitch/LightSwitch.component'
import { NavigationComponent } from './components/static/Navigation/Navigation.component'
import { OutputComponent } from './components/application/output/Output/Output.component'
import { SubtitleComponent } from './components/application/output/Subtitle/Subtitle.component'
import { ToolsComponent } from './components/application/tools/Tools/Tools.component'
import { DropdownComponent } from './components/application/dropdowns/Dropdown/Dropdown.component'
import { TimeComponent } from './components/application/output/Time/Time.component'
import { FilterComponent } from './components/application/dropdowns/Filter/Filter.component'
import { SortComponent } from './components/application/dropdowns/Sort/Sort.component'
import { SelectComponent } from './components/application/dropdowns/Select/Select.component'
import { ActionsComponent } from './components/application/tools/Actions/Actions.component'
import { ReplaceComponent } from './components/application/tools/Replace/Replace.component'
import { ConsentComponent } from './components/static/Consent/Consent.component'
import { SubtitleEditorComponent } from './components/application/tools/SubtitleEditor/SubtitleEditor.component'
import { DragUploadComponent } from './components/application/output/DragUpload/DragUpload.component'
import { NotificationComponent } from './components/application/notifications/Notification/Notification.component'
import { NotifierComponent } from './components/application/notifications/Notifier/Notifier.component'
import { ToolsInputComponent } from './components/application/tools/ToolsInput/ToolsInput.component'
import { ShiftTimeComponent } from './components/application/tools/ShiftTIme/ShiftTime.component'
import { FileService } from './services/FileService.service';
import { ApplicationService } from './services/ApplicationService.service';
import { SubtitleService } from './services/SubtitleService.service';
import { CacheService } from './services/CacheService.service';
import { SelectionService } from './services/SelectionService.service';
import { FilterService } from './services/FilterService.service';

const routes: Routes = [
  { path: '', component: HomePage },
  { path: 'about', component: AboutPage }
]

@NgModule({
  declarations: [
    HomePage,
    AboutPage,
    AppComponent,
    ConsentComponent,
    ShiftTimeComponent,
    ToolsInputComponent,
    DragUploadComponent,
    NavigationComponent,
    LightSwitchComponent,
    NotifierComponent,
    NotificationComponent,
    OutputComponent,
    SubtitleComponent,
    ToolsComponent,
    DropdownComponent,
    TimeComponent,
    FilterComponent,
    SortComponent,
    SelectComponent,
    SubtitleEditorComponent,
    ActionsComponent,
    ReplaceComponent
  ],
  imports: [
    RouterModule.forRoot(routes),

    BrowserModule,
    FormsModule
  ],
  providers: [
    ApplicationService,
    CacheService,
    ClickChecker,
    ChangesService,
    FileService,
    FilterService,
    NotifierService,
    SubtitleService,
    SelectionService
  ],
  entryComponents: [ SortComponent, FilterComponent ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
