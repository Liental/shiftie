import { Component, HostListener } from '@angular/core'
import { ClickChecker } from '../services/ClickChecker.service'
import { ApplicationService } from '../services/ApplicationService.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor (public application: ApplicationService, private clickChecker: ClickChecker) {

  }

  ngOnInit () {
    this.application.hasConsent = localStorage.consent === 'true'
  }

  @HostListener('document:click', [ '$event' ])
  onClick = (event: Event) => {
    this.clickChecker.clickEvent(event)
  }
}
