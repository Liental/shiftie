import { Component, HostListener } from '@angular/core'
import { FileService } from 'src/app/services/FileService.service'
import { SelectionService } from 'src/app/services/SelectionService.service'
import { ApplicationService } from 'src/app/services/ApplicationService.service'

@Component({
  templateUrl: './Home.component.html',
  styleUrls: [ './Home.component.css' ]
})
export class HomePage {
  timeoutID: number

  constructor (private application: ApplicationService, private selection: SelectionService, private fileManager: FileService) {

  }

  @HostListener('document:dragover', [ '$event' ])
  onDragOver = (event: DragEvent) => {
    const items = event.dataTransfer.items
  
    if (items[0].kind === 'file') {
      this.application.settings.isDragging = true
      clearTimeout(this.timeoutID)
    }

    this.timeoutID = window.setTimeout(() => this.application.settings.isDragging = false, 100)
  }

  @HostListener('window:beforeunload', [ '$event' ])
  beforeExit = (e: BeforeUnloadEvent) => {
    const originalDisplay = this.application.srtFile.displayRaw()
    const newDisplay = this.application.newSrtFile.displayRaw()

    if (originalDisplay === newDisplay)
      return true
    
    e.preventDefault()
    e.returnValue = true
  }

  @HostListener('document:keyup', [ '$event' ])
  onKeyUp = (event: KeyboardEvent) => {
    if (event.key === 's' && event.ctrlKey) {
      this.fileManager.writeFile()
      return false
    }
  }

  @HostListener('document:keydown', [ '$event' ])
  checkKeyDown = (event: KeyboardEvent) => {
    if (!event.ctrlKey)
      return

    if (event.key === 's')
      return false

    switch (event.code) {
      case 'Minus':
        this.selection.removeSelected()
        return false
      case 'Equal':
        this.application.settings.isEditing = true
        window.scrollTo({ top: 0, behavior: 'smooth' })
        return false
    }
  }
}