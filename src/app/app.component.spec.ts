import { TestBed } from '@angular/core/testing'
import { AppComponent } from './root/app.component'

describe('AppComponent', () => {
  beforeEach(() => TestBed.configureTestingModule({ declarations: [ AppComponent ] }).compileComponents())
})
