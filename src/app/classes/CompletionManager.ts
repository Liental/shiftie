import { Subtitle } from '../modules/Subtitle'

export class ContentManager {
  /**
   * Finds all words that start with given base string
   * @param base - a string of text that the words should begin with
   * @param subtitles - set of subtitles to look for a word in
  */
  public static getWordBank = (base: string, subtitles: Subtitle[]) => {
    const contents = []
    const foundSubtitles = subtitles.filter((subtitle) => subtitle.content.includes(base))

    for (const subtitle of foundSubtitles) {
      const words = subtitle.content.split(' ')
      
      for (const word of words) {
        if (word.startsWith(base))
          contents.push(word)
      }
    }

    return contents
  }

  /**
  * Get the number of occurances in all the subtitles for given string
  * @param word - string to look for in the contents
 */
  public static getWordsCount = (word: string, subtitles: Subtitle[], isRegex: boolean = false) => {
    let results = 0
    const contents = subtitles.map((subtitle) => subtitle.content).join(' ')
    
    if (isRegex && word) {
      try { 
        const regex = new RegExp(word) 
        results = contents.split(regex).length - 1
      } catch { results = -1 } 
    } else {
      results = contents.split(word).length - 1
    }

    return results
  }

  /**
   * Get suggestion of a word based on the provided string
   * @param base - string of text to complete
   * @param subtitles - set of subtitles to look for a suggestion in
  */
  public static getSuggestion = (base: string, subtitles: Subtitle[]) => {
    const wordBank = ContentManager.getWordBank(base, subtitles)
    const unique = wordBank.filter((word, index) => wordBank.indexOf(word) === index)
    const sorted = unique.sort((a, b) => ContentManager.getWordsCount(a, subtitles) < ContentManager.getWordsCount(b, subtitles) ? 1 : -1)

    return sorted[0]
  }
}