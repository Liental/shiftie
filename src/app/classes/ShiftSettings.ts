export class ShiftSettings {
  public backwards?: boolean = false
  public shiftStart?: boolean = true
  public shiftEnd?: boolean = true
}