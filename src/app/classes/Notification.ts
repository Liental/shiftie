export enum NotificationType {
  INFO = 'info',
  INPUT = 'input',
  ERROR = 'error',
  WARNING = 'warning',
  QUESTION = 'question'
}

export class Notification {
  public content: string
  public duration?: number = 2500
  public type?: NotificationType = NotificationType.INFO

  constructor (content: string, type: NotificationType = NotificationType.INFO, duration: number = 2500) {
    this.type = type
    this.content = content
    this.duration = duration
  }
}

export class InputNotification extends Notification {
  public onSubmit: any = () => { }
  public inputValue: string = ''

  constructor (content: string) {
    super(content, NotificationType.INPUT, NaN)
  }
}

export class QuestionNotification extends Notification {
  public onSubmit: any = () => { }

  constructor (content: string) {
    super(content, NotificationType.QUESTION, NaN)
  }
}