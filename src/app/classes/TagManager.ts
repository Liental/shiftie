import { Subtitle } from '../modules/Subtitle'
import { SubtitleChange } from './SubtitleChange'

/** Enum containing styles and their tags */
export enum TagEnum {
  BOLD = 'b',
  ITALIC = 'i',
  FONT = 'font',
  UNDERLINE = 'u'
}

/** Settings used for tag operations */
export interface TagSettings {
  /** The tag that the operation is about */
  tag: TagEnum

  /** Where should the content start and end */
  selection?: TagSelection

  /** Additional paramenters for a tag */
  parameters?: TagParameter[]
}

/** An interfact containing selection start and end numbers */
export interface TagSelection {
  /** Where should the selection start */
  start: number

  /** Where should the selection end */
  end: number
}

/** Tag parameter inteface containing the name and value of a paramenter */
export interface TagParameter {
  /** The name of a paramenter */
  name: string

  /** The value of a parameter */
  value: string
}

/** A manager taking care of tags operations */
export class TagManager {
  /**
   * Display given parameters in a pretty form
   * @param parameters - parameters to display
  */
  public static getParamsDisplay (parameters: TagParameter[]) {
    if (!parameters || parameters.length === 0)
      return ''

    const params = parameters.map((param) => `${param.name}="${param.value}"`)
    return ' ' + params.join(' ')
  }

  /**
   * Checks if given content is already tagged with given tag
   * @param content - content to be checked
   * @param settings - object containing settings about given tag
  */
  public static containsTag (content: string, settings: TagSettings) {
    const selectionEnd = settings.selection ? settings.selection.start : 0
    const selectionStart = settings.selection ? settings.selection.end : content.length
    
    const selected = content.substring(selectionStart, selectionEnd)
    const paramsDisplay = this.getParamsDisplay(settings.parameters)
    const tag = settings.tag
    
    const start = selected.startsWith(`<${tag}>`) || (selected.startsWith(`<${tag + paramsDisplay}>`))
    return start && selected.endsWith(`</${tag}>`)
    
  }

  /**
   * Changes tags of given subtitles and returns the changes object
   * @param subtitles - subtitles to be changed
   * @param settings - object containing settings about given tag
  */
  public static changeTag (subtitles: Subtitle[], settings: TagSettings) {
    const changes = subtitles.map((subtitle) => {
      const clone = subtitle.clone()
      settings.selection = { start: 0, end: subtitle.content.length } 
      
      if (!TagManager.containsTag(subtitle.content, settings))
        subtitle.content = TagManager.addTags(subtitle.content, settings)
      else
        subtitle.content = TagManager.removeTags(subtitle.content, settings)

      return new SubtitleChange(clone, subtitle)
    })

    return changes
  }

  /**
   * Removes given tag from selected part of given content
   * @param content - content to be checked
   * @param settings - object containing settings about given tag
  */
  public static removeTags (content: string, settings: TagSettings) {
    if (settings.selection === undefined)
      settings.selection = { start: 0, end: content.length }  

    const start = settings.selection.start
    const end = settings.selection.end

    const paramsDisplay = TagManager.getParamsDisplay(settings.parameters)
    const selectedIndex = (2 + settings.tag.length + paramsDisplay.length)

    let result = content.substring(0, start)
    result += content.substring(start + selectedIndex, end - (3 + settings.tag.length))
    result += content.substring(end, content.length)

    return result
  }

  /**
   * Adds given tag to selected part of given content
   * @param content - content to be checked
   * @param settings - object containing settings about given tag
  */
  public static addTags (content: string, settings: TagSettings) {
    if (settings.selection === undefined)
      settings.selection = { start: 0, end: content.length }  

    const start = settings.selection.start
    const end = settings.selection.end
    const selected = content.substring(start, end)
    const paramsDisplay = TagManager.getParamsDisplay(settings.parameters)
    
    let result = content.substring(0, start) 
    result += `<${settings.tag + paramsDisplay}>` + selected + `</${settings.tag}>`
    result += content.substring(end, content.length)
    
    return result
  }
}