/** Class containing static methods used to generate IDs */
export class IDGenerator {
  public static numbers: string = '0123456789' 
  public static lowercase: string = 'abcdefghijklmnopqrstuwvxyz'
  public static uppercase: string = IDGenerator.lowercase.toUpperCase()
  
  /** 
   * Returns an alphabet generated based on given settings
   * @param uppercase - boolean defining if generated alphabet should contain uppercase letters
   * @param lowercase - boolean defining if generated alphabet should contain lowercase letters
   * @param numbers - boolean defining if generated alphabet should contain number
  */
  public static generateAlphabet = (uppercase: boolean = true, lowercase: boolean = true, numbers: boolean = true) => {
    let result = ''

    if (uppercase)
      result += IDGenerator.uppercase

    if (lowercase)
      result += IDGenerator.lowercase

    if (numbers)
      result += IDGenerator.numbers

    return result
  }

  /**
   * Returns an ID generated based on given settings
   * @param length - length of one part of the ID
   * @param parts - number of parts the ID should consist of
  */
  public static generateID = (length: number = 5, parts: number = 5) => {
    let idResult = ''
    const alphabet = IDGenerator.generateAlphabet()

    for (let i = 0; i < parts; i++) {
      for (let j = 0; j < length; j++)
        idResult += alphabet[Math.floor(Math.random() * alphabet.length)]        

      idResult += i < parts - 1 ? '-' : ''
    }

    return idResult
  }
}