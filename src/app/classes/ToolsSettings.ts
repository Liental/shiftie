export class ToolsSettings {
  /** Defines if the application should automatically sort the subtitles by their start time */
  public isCleaning = true

  /** Defines if the subtitle editor should be openned */
  public isEditing = false

  /** Defines if the user is dragging a file around the application */
  public isDragging = true

  /** Defines if the time shifting tool should be opened */
  public isShifting = false

  /** Defines if the replacing tool should be opened */
  public isReplacing = false
  
  /** Defines if the subtitles should be added in from-to mode */
  public isFromTo = false

  /** Defines if the subtitles should be selected or added to selection */
  public isSelecting = false

  /** Set all the values in the settings to their initial ones */
  public reset () {
    this.isCleaning = true
    this.isEditing = false
    this.isDragging = false
    this.isSelecting = false
    this.isReplacing = false
    this.isFromTo = false
    this.isSelecting = false
  }
  
  /** Switch clean up mode for subtitles */
  public switchCleaning () {
    this.isCleaning = !this.isCleaning
  }

  /** Switch editing mode for subtitles */
  public switchEditing () {
    this.isFromTo = false
    this.isShifting = false
    this.isReplacing = false
    this.isSelecting = false
    this.isEditing = !this.isEditing
  }

  /** Switch shifting mode on or off */
  public switchShifting () {
    this.isEditing = false
    this.isReplacing = false
    this.isShifting = !this.isShifting
  }
  
  /** Switch replacing mode on or off */
  public switchReplacing () {
    this.isEditing = false
    this.isShifting = false
    this.isReplacing = !this.isReplacing
  }

  /** Switches from-to selection mode and turns off single file selection */
  public switchFromTo = () => {
    this.isSelecting = false
    this.isFromTo = !this.isFromTo
  }

  /** Switches single file selection mode and turns off from-to selection */
  public switchSelecting = () => {
    this.isFromTo = false
    this.isSelecting = !this.isSelecting
  }
}