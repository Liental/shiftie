import { Subtitle } from '../modules/Subtitle'
import { IDGenerator } from './IDGenerator'

/** 
 * Class used to define a subtitle change, holds a set of two subtitles concerning the change 
 * @param from - state of given subtitle before a change
 * @param to - state of given subtitle after a change
*/
export class SubtitleChange {
  /** Change unique ID */
  public id?: string

  /** Subtitle after a change was made, if this field is undefined that means given subtitle was deleted */
  public subtitleTo: Subtitle
  
  /** Subtitle before a change was made, if this field is undefined that means given subtitle was just created */
  public subtitleFrom: Subtitle

  constructor (from: Subtitle, to: Subtitle) {
    this.subtitleFrom = from && from.clone()
    this.subtitleTo = to && to.clone()

    this.id = IDGenerator.generateID()
  }
}