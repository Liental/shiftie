import { Component, ViewChild } from '@angular/core'
import { ApplicationService } from 'src/app/services/ApplicationService.service'

@Component({
  selector: 'light-switch',
  templateUrl: './LightSwitch.component.html',
  styleUrls: [ './LightSwitch.component.css' ]
})
export class LightSwitchComponent {
  @ViewChild('lightSwitch') lightSwitch
  isDark: boolean = true

  constructor (private application: ApplicationService) { }

  ngOnInit () {
    // Because we're changing state in the switchLight method
    this.isDark = (this.application.hasConsent && localStorage.isDark === 'false')
    this.switchLight()
  }

  switchLight = () => {
    this.isDark = !this.isDark

    if (this.application.hasConsent)
      localStorage.isDark = this.isDark

    document.body.className = this.isDark ? 'dark' : ''
    this.lightSwitch.nativeElement.className = this.isDark ? 'fa fa-sun' : 'fa fa-moon' 
  }
}