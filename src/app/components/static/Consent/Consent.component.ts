import { Component } from '@angular/core'
import { ApplicationService } from 'src/app/services/ApplicationService.service';

@Component({
  selector: 'consent',
  templateUrl: './Consent.component.html',
  styleUrls: [ './Consent.component.css' ]
})
export class ConsentComponent {
  isAnswered = false
  
  constructor (private application: ApplicationService) { }

  setConsent = (accept: boolean) => {
    if (accept) {
      localStorage.consent = true
      this.application.hasConsent = true
    } else {
      localStorage.clear()
    }

    this.isAnswered = true
  }
}