import { Component, ViewChild, Input } from '@angular/core'
import { TimeComponent } from 'src/app/components/application/output/Time/Time.component'
import { NotifierService } from 'src/app/services/NotifierService.service'
import { FilterService } from 'src/app/services/FilterService.service'
import { ApplicationService } from 'src/app/services/ApplicationService.service'

@Component({
  selector: 'filter',
  templateUrl: './Filter.component.html',
  styleUrls: [ './Filter.component.css' ]
})
export class FilterComponent {
  @Input() id
  @Input() content
  @ViewChild('firstElement') firstElement
  @ViewChild('startTime') startTime: TimeComponent
  @ViewChild('endTime') endTime: TimeComponent

  constructor (private filter: FilterService, private application: ApplicationService, private notifier: NotifierService) {

  }

  reset = () => {
    this.id = ''
    this.content = ''
    
    this.startTime.reset()
    this.endTime.reset()
    this.notifier.addInfoNotification('Successfully cleared the filter')
  }

  focusFirst = () => {
    this.firstElement.nativeElement.focus()
  }

  checkNumber = (event: KeyboardEvent) => {
    const isNotNumber = isNaN(parseInt(event.key))
    const isSpecialKey = event.keyCode > 7 && event.keyCode < 10

    if (isNotNumber && !isSpecialKey)
      event.preventDefault()
  }

  checkInput = (event: KeyboardEvent) => {
    if (event.key === 'Enter') 
      this.setData()
  }

  setData = () => {
    const id = parseInt(this.id)
    const filter = this.filter.manager
    
    filter.content = this.content
    filter.id = isNaN(id) ? undefined : id
    filter.startTime = this.startTime.getTime(true)
    filter.endTime = this.endTime.getTime(true)
    
    this.application.position = this.application.getHalfLimit()
    this.notifier.addInfoNotification('Successfully filtered the subtitles')
  }
}
