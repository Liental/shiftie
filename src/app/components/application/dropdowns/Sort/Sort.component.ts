import { Component, ViewChild } from '@angular/core'
import { Subtitle } from 'src/app/modules/Subtitle'
import { FilterService } from 'src/app/services/FilterService.service';

@Component({
  selector: 'sort',
  templateUrl: './Sort.component.html',
  styleUrls: [ './Sort.component.css' ]
})
export class SortComponent {
  @ViewChild('sortSelect') sortSelect
  @ViewChild('orderSelect') orderSelect

  sortOptions = [ 
    { value: (s: Subtitle) => s.id, display: 'id' },
    { value: (s: Subtitle) => s.startTime.getMilliseconds(), display: 'time' },
    { value: (s: Subtitle) => s.getDuration(), display: 'duration' },
    { value: (s: Subtitle) => s.content.length, display: 'length' },
    { value: (s: Subtitle) => s.content[0], display: 'content' }
  ]

  orderOptions = [
    { value: 1, display: 'ascending' },
    { value: -1, display: 'descending' }
  ]

  constructor (private filter: FilterService) {

  }

  reset = () => {
    this.sortSelect.selectedOption = this.sortOptions[0]
    this.orderSelect.selectedOption = this.orderOptions[0]
  }

  setData = () => {
    if (this.sortSelect.selectedOption)  
      this.filter.manager.sortClosure = this.sortSelect.selectedOption.value

    if (this.orderSelect.selectedOption)
      this.filter.manager.orderBy = this.orderSelect.selectedOption.value
  }
}
