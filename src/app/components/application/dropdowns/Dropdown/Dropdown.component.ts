import { Component, Input, ViewChild, HostListener } from '@angular/core'
import { ClickChecker } from 'src/app/services/ClickChecker.service'

@Component({
  selector: 'dropdown',
  templateUrl: './Dropdown.component.html',
  styleUrls: [ './Dropdown.component.css' ]
})
export class DropdownComponent {
  revealed = false
  @Input() type: string
  @ViewChild('title') title
  @ViewChild('dropdown') dropdown
  @ViewChild('container') container

  constructor (private checker: ClickChecker) {

  }

  ngOnInit () {
    this.checker.dropdownCheck.push(this)
  }

  ngAfterViewInit () {
    this.dropdown.nativeElement.classList.add('active')
    this.changeTitleWidth()
    this.dropdown.nativeElement.classList.remove('active')
  }

  ngAfterContentChecked () {
    this.changeTitleWidth()
  }

  changeTitleWidth = () => {
    const titleElement = this.title.nativeElement
    const style = getComputedStyle(titleElement)
    const offset = this.container.nativeElement.offsetWidth
    const padding = parseInt(style.paddingLeft.replace('px', ''))

    titleElement.style.width = offset - (padding ? padding * 2 : 0) - 2 + 'px'
  }

  @HostListener('keyup', [ '$event' ])
  onKeyUp = (event: KeyboardEvent) => {
    if (event.key === 'Escape')
      this.revealed = false
  }
}