import { Component, Input, ViewChild } from '@angular/core'
import { Option } from 'src/app/modules/Option'
import { ClickChecker } from 'src/app/services/ClickChecker.service'

@Component({
  selector: 'selector',
  templateUrl: './Select.component.html',
  styleUrls: [ './Select.component.css' ]
})
export class SelectComponent {
  revealed = false
  selectedOption: Option
  @Input() options: Option[] = []
  @ViewChild('select') select

  constructor (private checker: ClickChecker) {

  }

  ngOnInit () {
    this.checker.selectCheck.push(this)
  }

  selectValue = (option: Option) => {
    this.selectedOption = option
  }
}
