import { Component, Input, ViewChild, ViewChildren, HostListener } from '@angular/core'
import { Time } from 'src/app/modules/Time'

@Component({
  selector: 'time',
  templateUrl: './Time.component.html',
  styleUrls: [ './Time.component.css' ]
})
export class TimeComponent {
  @Input() time: Time = new Time()
  @ViewChild('hoursInput') hoursInput
  @ViewChildren('timeValue') values

  @HostListener('paste', [ '$event' ])
  onPaste = (event: ClipboardEvent) => {
    const clipboard = event.clipboardData.getData('text')
    
    if (clipboard.match(/\d\d:\d\d:\d\d,\d\d\d/))
      this.time = new Time(clipboard)

    event.preventDefault()
  }

  @HostListener('copy', [ '$event' ])
  onCopy = (event: ClipboardEvent) => {
    const time = this.getTime()
    
    event.clipboardData.setData('text', time.display())
    event.preventDefault()
  }

  getUnit = (unit: string) => {
    const result = this.replaceEmpty(this.time[unit])
    return result
  }
  
  replaceEmpty = (unit: number) => {
    return unit > 0 ? unit : ''
  }

  focusFirst = () => {
    this.hoursInput.nativeElement.focus()
  }

  reset = () => {
    this.time = new Time()
  }

  checkPrevious = (event: KeyboardEvent) => {
    const field = event.srcElement as HTMLInputElement
    const isSelected = field.selectionStart !== field.selectionEnd

    if (field.value.length === 0 && event.keyCode === 8 && !isSelected) {
      const previous = field.previousElementSibling as HTMLInputElement

      if (previous) {
        previous.focus()
        previous.value = previous.value.substring(0, previous.value.length - 1)
        return false
      }
    }
  }

  checkField = (event: KeyboardEvent) => {
    const field = event.srcElement as HTMLInputElement

    const isNotNumber = isNaN(parseInt(event.key))
    const isSelected = field.selectionStart !== field.selectionEnd
    const isSpecialKey = event.keyCode > 7 && event.keyCode < 10

    if (field.value.length >= field.placeholder.length && !isNotNumber && !isSpecialKey && !isSelected) {
      const next = field.nextElementSibling as HTMLInputElement

      if (next && next.value.length < next.placeholder.length)
        next.value += event.key
      
      if (next) {
        next.focus()
        event.preventDefault()
      }
    }

    if ((field.value.length >= field.placeholder.length || isNotNumber) && !isSpecialKey && !isSelected)
      event.preventDefault()
  }

  getTime = (toCompare?: boolean) => {
    const getValue = (element: any, a: string) => {
      const value = element.nativeElement.value
      
      const isEmpty = value.length === 0
      const timeValue = (isEmpty && toCompare) ? -1 : value
      
      return timeValue + a
    }

    const results = this.values._results
    const timeString = getValue(results[0], ':') + getValue(results[1], ':')
                     + getValue(results[2], ',') + getValue(results[3], '') 

    return new Time(timeString)
  }
}