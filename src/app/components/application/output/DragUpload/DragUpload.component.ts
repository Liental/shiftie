import { Component, HostListener } from '@angular/core'
import { FileService } from 'src/app/services/FileService.service'

@Component({
  selector: 'drag-upload',
  templateUrl: './DragUpload.component.html',
  styleUrls: [ './DragUpload.component.css' ]
})
export class DragUploadComponent {
  constructor (private fileManager: FileService) {

  }

  @HostListener('dragover', [ '$event' ])
  onDragOver = (event: DragEvent) => {
    event.preventDefault()
  }

  @HostListener('drop', [ '$event' ])
  onDrop = (event: DragEvent) => {
    const file = event.dataTransfer.files[0]
    this.fileManager.loadFile(file)
    event.preventDefault()
  }

  loadFile = (event: Event) => {
    const element = event.target as HTMLInputElement
    const file = element.files[0]

    if (file)
      this.fileManager.loadFile(file)
  }
}