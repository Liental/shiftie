import { Component, ViewChild, HostListener, ElementRef, ViewChildren } from '@angular/core'
import { SrtFile } from 'src/app/modules/SrtFile'
import { NotifierService } from 'src/app/services/NotifierService.service'
import { ApplicationService } from 'src/app/services/ApplicationService.service'
import { SelectionService } from 'src/app/services/SelectionService.service';
import { CacheService } from 'src/app/services/CacheService.service';
import { SubtitleService } from 'src/app/services/SubtitleService.service';
import { ChangesService } from 'src/app/services/ChangesService.service';
import { TagManager, TagSettings, TagEnum } from 'src/app/classes/TagManager';
import { InputNotification } from 'src/app/classes/Notification';

@Component({
  selector: 'srt-output',
  templateUrl: './Output.component.html',
  styleUrls: [ './Output.component.css' ]
})
export class OutputComponent {
  @ViewChild('tools') tools
  @ViewChild('output') output
  @ViewChild('toolsInput') toolsInput
  @ViewChild('tools', { read: ElementRef }) toolsElement
  @ViewChild('toolsInput', { read: ElementRef }) toolsInputElement
  @ViewChildren('newSubtitle', { read: ElementRef }) newSubtitles
  currentTemporary: HTMLElement
  lastScrollPosition: number = 0

  constructor (private application: ApplicationService, private notifier: NotifierService, private selection: SelectionService, private cache: CacheService,
               private subtitleManager: SubtitleService, private changeManager: ChangesService) {

  }

  ngAfterViewChecked () {
    if (!this.toolsInputElement || !this.toolsElement)
      return
    
    const srtEditor = this.output.nativeElement
    const inputOffset = this.toolsInputElement.nativeElement.offsetHeight
    const toolsOffset = this.toolsElement.nativeElement.children[0].offsetHeight
    const height = `calc(100% - ${inputOffset + toolsOffset}px)`

    srtEditor.style.height = height
    srtEditor.style.maxHeight = height
    srtEditor.style.paddingTop = inputOffset + 'px'

    for (const element of srtEditor.children) {
      element.style.height = height
      element.style.maxHeight = height
    }
  }

  isUploading = () => {
    return this.application.settings.isDragging
  }

  scrollTo = (bottom?: boolean) => {
    const output: HTMLElement = this.output.nativeElement
    const subtitles = this.application.newSrtFile.subtitles

    if (bottom) {
      const length = this.application.getLength()

      this.application.position = length
      this.selection.setTemporary(subtitles[length - 1])

      output.scrollTo(0, output.scrollHeight)
      return
    } 
 
    this.application.position = this.application.getHalfLimit()
    this.selection.setTemporary(subtitles[0])
    this.output.nativeElement.scrollTo(0, 0)
  }


  getSubtitles = (isNew?: boolean) => {
    const subtitles = this.cache.getCachedSubtitles()
    
    if (this.application.position === 0) {
      this.application.position = this.application.getHalfLimit()
      this.output.nativeElement.scrollTo(0, 0)
    } 
    
    return subtitles[isNew ? 1 : 0]
  }
  
  @HostListener('scroll', [ '$event' ])
  onScroll = () => {
    const output = this.output.nativeElement
    const maxHeight = output.scrollHeight
    const scroll = output.scrollTop
    const halfLimit = this.application.getHalfLimit()

    if ((scroll + 1000 >= maxHeight && scroll > this.lastScrollPosition) && this.application.position + halfLimit < this.cache.filteredLength) {
      this.lastScrollPosition = scroll
      this.application.position += halfLimit
      return true
    }

    if ((scroll > 0 && scroll < 1000 && scroll <= this.lastScrollPosition) && this.application.position > halfLimit) {
      this.lastScrollPosition = scroll
      this.application.position -= halfLimit
      return true
    }
  }

  setTemporarySelect = (forwards: boolean = true) => {
    const cachedSubtitles = this.cache.getCachedSubtitles()
    const subtitles = cachedSubtitles[1]
    const toMove = forwards ? 1 : -1

    if (!this.selection.getTemporary()) {
      this.selection.setTemporary(subtitles[0])
      return
    }

    const subtitleElements = Array.from(this.newSubtitles._results)
                                  .map((subtitle: ElementRef) => subtitle.nativeElement.children[0])

    const tempElement = subtitleElements.filter((elem: HTMLElement) => elem.classList.contains('temporary'))[0]
    
    const index = subtitleElements.indexOf(tempElement) + toMove 
    const nextElement = subtitleElements[index]

    if (nextElement) {
      nextElement.scrollIntoView()
      this.selection.setTemporary(subtitles[index])
    }
  }

  onClick = (event: Event) => {
    const target = event.target as HTMLElement
    const output = this.output.nativeElement as HTMLElement

    if (target === output || target.nodeName === 'SUBTITLE' || target.classList.contains('srt-display')) {
      this.selection.setTemporary(undefined)
      this.selection.manager.selected = []
    }
  }

  onCut = (event: ClipboardEvent) => {
    this.onCopy(event)
    this.selection.removeSelected()

    return false
  }

  onCopy = (event: ClipboardEvent) => {
    const clipboard = this.subtitleManager.copySubtitles()
    
    event.clipboardData.setData('text', clipboard)
    event.preventDefault()
    return false
  }

  onPaste = (event: ClipboardEvent) => {
    const clipboard = event.clipboardData.getData('text')
    const srt = new SrtFile(clipboard.trim())
  
    this.subtitleManager.addSubtitles(srt.subtitles)
    this.selection.manager.selected = [ ]

    return false
  }

  private addTags (settings: TagSettings) {
    const selected = this.selection.manager.selected
    const subtitles = selected.length === 0 ? this.application.newSrtFile.subtitles : selected
    const changes = TagManager.changeTag(subtitles, settings)

    this.changeManager.addChanges(changes)
    this.notifier.addInfoNotification(`Applied tag change to ${changes.length} subtitle${changes.length !== 1 ? 's' : ''}`)
  }

  onKeyUp = (event: KeyboardEvent) => {
    switch (event.key) {
      case 'Escape':
        this.selection.setTemporary(undefined)
        this.application.settings.isEditing = false
        this.selection.manager.selected = []
        return false
      case 'Alt':
        this.application.settings.switchSelecting()
        return false
      case 'Shift':
        this.application.settings.switchFromTo()
        return false
      case 'b': case 'i': case 'u':
        const settings: TagSettings = { tag: event.key as TagEnum }
        this.addTags(settings)
        return false
      case 'Home':  
        this.scrollTo()
        return false
      case 'End':
        this.scrollTo(true)
        return false
      case 'h':
        this.application.settings.switchReplacing()
        return false
      case 't':
        this.application.settings.switchShifting()
        return false
      case 'f':
        if (event.ctrlKey) {
          this.tools.sortDropdown.revealed = true
          this.tools.filterDropdown.revealed = true
          setTimeout(() => { this.tools.filter.focusFirst() }, 50)
          return false
        }

        const notification = new InputNotification('Enter the color you want the selected subtitles to be')
        
        notification.onSubmit = () => {
          const settings: TagSettings = { tag: TagEnum.FONT, parameters: [ { name: 'color', value: notification.inputValue } ] }
          this.addTags(settings)
        }

        this.notifier.addNotification(notification)
        return false
      case 'c':
        if (!event.ctrlKey)
          this.tools.cancelFilter()
        return false
    }
  }

  onKeyPress = (event: KeyboardEvent) => {
    switch (event.code) {
      case 'KeyA':
        this.selection.selectAll()
        return false
      case 'KeyR': case 'Delete':
        this.selection.removeSelected()
        return false
      case 'Space': case 'Enter':
        this.subtitleManager.chooseSubtitle(this.selection.getTemporary())
        return false
    }
  }

  onKeyDown = (event: KeyboardEvent) => {
    switch (event.code) {
      case 'ArrowUp':
        this.setTemporarySelect(false)
        return false
      case 'ArrowDown':
        this.setTemporarySelect()
        return false
      case 'KeyY':
        if (event.ctrlKey) {
          this.changeManager.popChanges(false)
          return false
        }
      case 'KeyZ':
        if (event.ctrlKey && event.altKey) {
          this.changeManager.popChanges(false)
          return false
        }

        if (event.ctrlKey) {
          this.changeManager.popChanges()
          return false
        }

        break
      case 'KeyF':
        if (event.ctrlKey)
          return false
        break
    }
  }
}
