import { Component, Input } from '@angular/core'
import { Subtitle } from 'src/app/modules/Subtitle'
import { SelectionService } from 'src/app/services/SelectionService.service'

@Component({
  selector: 'subtitle',
  templateUrl: './Subtitle.component.html',
  styleUrls: [ './Subtitle.component.css' ]
})
export class SubtitleComponent {
  @Input() selectable = false
  @Input() subtitle: Subtitle

  constructor (private selection: SelectionService) {

  }

  isSelected = () => {
    if (this.subtitle === undefined || !this.selectable) 
      return false 

    const result = this.selection.manager.isSelected(this.subtitle)
    return result
  }

  isTemporary = () => {
    const temporarySubtitle = this.selection.getTemporary()
    const isEqual = temporarySubtitle && this.subtitle.id === temporarySubtitle.id 

    return this.selectable && isEqual
  }

  chooseSubtitle = () => {
    if (!this.selectable)
      return

    this.selection.chooseSubtitle(this.subtitle)
  }

  stripHTML = (str: string) => {
    let result = str
    const htmlStrip = [ 'font', '/font' ]

    for (const html of htmlStrip) {
      const tag = '<' + html

      while (result.includes(tag)) {
        const start = result.indexOf(tag)
        const end = result.indexOf('>', start) + 1
        const fontDef = result.substr(start, end - start)
          
        result = result.replace(fontDef, '')
      }
    }

    return result
  }

  replaceBreaks = (str: string) => {
    return str.split('\r\n').join('<br>')
  }
}