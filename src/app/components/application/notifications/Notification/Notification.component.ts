import { Component, Input } from '@angular/core'
import { Notification, NotificationType } from 'src/app/classes/Notification'
import { NotifierService } from 'src/app/services/NotifierService.service'

@Component({
  selector: 'notification',
  templateUrl: './Notification.component.html',
  styleUrls: [ './Notification.component.css' ]
})
export class NotificationComponent {
  NotificationType = NotificationType
  @Input() notification: Notification

  constructor (public notifier: NotifierService) {

  }
}