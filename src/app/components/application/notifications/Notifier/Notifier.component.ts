import { Component } from '@angular/core'
import { NotifierService } from 'src/app/services/NotifierService.service'

@Component({
  selector: 'notifier',
  templateUrl: './Notifier.component.html',
  styleUrls: [ './Notifier.component.css' ]
})
export class NotifierComponent {
  constructor (public notifier: NotifierService) {

  }
}