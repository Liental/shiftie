import { Component, ViewChild } from '@angular/core'
import { FilterService } from 'src/app/services/FilterService.service'
import { CacheService } from 'src/app/services/CacheService.service'
import { NotifierService } from 'src/app/services/NotifierService.service'
import { ApplicationService } from 'src/app/services/ApplicationService.service'

@Component({
  selector: 'tools',
  templateUrl: './Tools.component.html',
  styleUrls: [ './Tools.component.css' ]
})
export class ToolsComponent {
  @ViewChild('sort') sort
  @ViewChild('filter') filter
  @ViewChild('sortDropdown') sortDropdown
  @ViewChild('filterDropdown') filterDropdown

  constructor (public application: ApplicationService, private notifier: NotifierService, private filterManager: FilterService,
               private cacheManager: CacheService) {

  }

  changeCleanup = () => {
    this.application.settings.switchCleaning()

    if (this.application.settings.isCleaning) {
      this.notifier.addWarningNotification('Enabled automatic clean up, the subtitles will now be sorted by their time')
      this.application.sortByTime()
    } else {
      this.notifier.addWarningNotification('Disabled automatic clean up, from now on the subtitles will not be sorted')
    }
  }

  cancelFilter = () => {
    this.sort.reset()
    this.filter.reset()
    this.filterManager.manager.reset()
  }

  setLength = (mobile: boolean = false) => {
    const length = this.cacheManager.filteredLength
    const preview = length + (length !== 1 ? ' subtitles' : ' subtitle')

    return mobile ? length : preview
  }

  checkInput = (event: KeyboardEvent) => {
    const elem = this.filter.nativeElement

    if (event.key === 'Enter' && event.target === elem) 
      this.search()
  }

  search = () => {
    this.sort.setData()
    this.filter.setData()
  }
}