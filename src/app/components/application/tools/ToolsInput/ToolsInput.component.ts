import { Component } from '@angular/core'
import { ApplicationService } from 'src/app/services/ApplicationService.service'
import { SelectionService } from 'src/app/services/SelectionService.service'

@Component({
  selector: 'tools-input',
  templateUrl: './ToolsInput.component.html',
  styleUrls: [ './ToolsInput.component.css' ]
})
export class ToolsInputComponent {
  constructor (public application: ApplicationService, public selection: SelectionService) {

  }

  isRevealed = () => {
    return (
      this.application.settings.isReplacing ||
      this.application.settings.isEditing ||
      this.application.settings.isShifting
    )
  }
}