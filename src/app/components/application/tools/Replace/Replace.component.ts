import { Component, Input, HostListener, ViewChild } from '@angular/core'
import { ContentManager } from 'src/app/classes/CompletionManager'
import { ApplicationService } from 'src/app/services/ApplicationService.service'
import { SubtitleService } from 'src/app/services/SubtitleService.service'

@Component({
  selector: 'replace-content',
  templateUrl: './Replace.component.html',
  styleUrls: [ './Replace.component.css' ]
})
export class ReplaceComponent {
  @Input() from: string
  @Input() to: string
  @ViewChild('fromInput') fromInput
  isRegex: boolean = false
  wordsFound: number = 0

  constructor (private application: ApplicationService, public subtitleManager: SubtitleService) { }

  public changeState () {
    this.application.settings.switchReplacing()

    if (this.application.settings.isReplacing)
      setTimeout(() => { this.fromInput.nativeElement.focus() }, 50)
    
    if (!this.application.settings.isReplacing) {
      this.to = ''
      this.from = ''
    }
  }

  public switchRegex () {
    this.isRegex = !this.isRegex
  }

  public findResults () {
    if (!this.from || this.from.length === 0)
      return
      
    const wordsFound = ContentManager.getWordsCount(this.from, this.application.newSrtFile.subtitles, this.isRegex)

    if (wordsFound !== -1)
      this.wordsFound = wordsFound
  }

  @HostListener('keyup', [ '$event' ])
  private onKeyUp = (ev: KeyboardEvent) => {
    if (ev.key === 'Enter') {
      this.subtitleManager.replace(this.from, this.to, this.isRegex)
      return false
    }
    
    if (ev.key === 'Escape') {
      this.changeState()
      return false
    }
  }
}