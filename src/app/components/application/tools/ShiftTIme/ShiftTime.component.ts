import { Component, ViewChild, HostListener } from '@angular/core'
import { SubtitleChange } from 'src/app/classes/SubtitleChange'
import { ChangesService } from 'src/app/services/ChangesService.service'
import { NotifierService } from 'src/app/services/NotifierService.service'
import { SelectionService } from 'src/app/services/SelectionService.service'
import { ApplicationService } from 'src/app/services/ApplicationService.service'
import { TimeComponent } from 'src/app/components/application/output/Time/Time.component'

@Component({
  selector: 'shift-time',
  templateUrl: './ShiftTime.component.html',
  styleUrls: [ './ShiftTime.component.css' ]
})
export class ShiftTimeComponent {
  @ViewChild('time') time: TimeComponent
  shiftStart: boolean = true
  shiftEnd: boolean = true

  constructor (public application: ApplicationService, private notifier: NotifierService, private selection: SelectionService,
               private changesManager: ChangesService) {

  }
  
  /** 
   * Shifts selected subtitles by the time defined by TimeComponent 
   * @param backwards - defines if the subtitles are meant to be shifted backwards
  */
  shiftSelected = (backwards?: boolean) => {
    const changes: SubtitleChange[] = []
    const settings = { backwards, shiftStart: this.shiftStart, shiftEnd: this.shiftEnd }

    const time = this.time.getTime()
    const selectedSubs = this.selection.manager.selected
    
    if (time.getMilliseconds() === 0 || (!this.shiftStart && !this.shiftEnd))
      return

    const subsToShift = selectedSubs.length > 0 ? selectedSubs : this.application.newSrtFile.subtitles 
    
    for(const subtitle of subsToShift) {
      const subBefore = subtitle.clone()
      const result = subtitle.shift(time, settings)

      if (result) {
        const change = new SubtitleChange(subBefore, subtitle.clone())
        changes.push(change)
      }
    }
    
    if (changes.length > 0) {
      this.application.sortByTime()
      this.changesManager.addChanges(changes)
      this.notifier.addInfoNotification(`Successfully shifted the time of ${changes.length} subtitle${changes.length !== 1 ? 's' : ''}`)
    }
  }

    /** Method used for keyup events, if the key is enter it loads file from input */
    @HostListener('keyup', [ '$event' ])
    checkInput = (event: KeyboardEvent) => {
      if (event.code !== 'Enter')
        return true
        
      this.shiftSelected()
      return false
    }
}