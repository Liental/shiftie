import { Component, ViewChild, HostListener, ElementRef } from '@angular/core'
import { SubtitleChange } from 'src/app/classes/SubtitleChange'
import { Subtitle } from 'src/app/modules/Subtitle'
import { ContentManager } from 'src/app/classes/CompletionManager'
import { NotifierService } from 'src/app/services/NotifierService.service'
import { SelectionService } from 'src/app/services/SelectionService.service'
import { ApplicationService } from 'src/app/services/ApplicationService.service'
import { SubtitleService } from 'src/app/services/SubtitleService.service';
import { ChangesService } from 'src/app/services/ChangesService.service';
import { TagManager, TagSettings, TagEnum } from 'src/app/classes/TagManager';

@Component({
  selector: 'subtitle-editor',
  templateUrl: './SubtitleEditor.component.html',
  styleUrls: [ './SubtitleEditor.component.css' ]
})
export class SubtitleEditorComponent {
  @ViewChild('endTime') endTime
  @ViewChild('startTime') startTime
  @ViewChild('content') content
  @ViewChild('suggestionContainer', { read: ElementRef }) suggestionContainer
  TagEnum = TagEnum
  wordCompletion: boolean = true
  subtitle: Subtitle

  constructor (public application: ApplicationService, private selection: SelectionService, private notifier: NotifierService,
               private subtitleManager: SubtitleService, private changesManager: ChangesService) { }
 
  ngOnInit () {
    const firstSelected = this.selection.manager.selected[0]
    this.subtitle = firstSelected ? firstSelected : new Subtitle()

    if (this.application.hasConsent)
      this.wordCompletion = localStorage.wordCompletion === 'true'
  }

  ngAfterViewInit () {
    this.startTime.focusFirst()
  }

  getModelSubtitle () {
    const subtitle = this.selection.manager.selected[0]
    return subtitle ? subtitle : this.subtitle
  }

  onInputChange () {
    this.suggestionContainer.nativeElement.children[0].innerText = this.content.nativeElement.value
    this.setSuggestion()
  }

  addTags = (settings: TagSettings) => {
    const elem = this.content.nativeElement as HTMLTextAreaElement
    const start = elem.selectionStart === elem.selectionEnd ? 0 : elem.selectionStart
    const end = elem.selectionStart === elem.selectionEnd ? elem.value.length : elem.selectionEnd
    
    settings.selection = { start, end }
    
    const tag = settings.tag
    const selectedContent = elem.value.substring(start, end)
    const paramsDisplay = TagManager.getParamsDisplay(settings.parameters)

    if (TagManager.containsTag(selectedContent, settings)) {
      elem.value = TagManager.removeTags(elem.value, settings)      
      elem.setSelectionRange(start, end - (5 + paramsDisplay.length + tag.length * 2))
    } else {
      elem.value = TagManager.addTags(elem.value, settings)
      elem.setSelectionRange(start, end + 5 + paramsDisplay.length + tag.length * 2)
    }

    this.suggestionContainer.nativeElement.children[0].innerText = elem.value
  }

  onScroll = () => {
    const contentElement = this.content.nativeElement
    const suggestionElement = this.suggestionContainer.nativeElement
    
    suggestionElement.children[0].style.height = contentElement.scrollHeight + 'px'
    suggestionElement.scrollTo(0, contentElement.scrollTop)
  }

  @HostListener('keydown', [ '$event' ])
  onKeyDown = (event: KeyboardEvent) => {
    if (event.ctrlKey && (event.key === 'i' || event.key === 'b' || event.key === 'u' || event.key === 'f'))
      return false
    
    if (event.code === 'Tab' && !event.shiftKey && event.target === this.content.nativeElement) {
      const contentElement = this.content.nativeElement
      const element = this.suggestionContainer.nativeElement
      const value = element.innerText

      if (value && value.length > 0 && value.length > contentElement.value.length)
        contentElement.value = value
   
      return false
    }
  }

  @HostListener('keyup', [ '$event' ])
  onKeyUp = (event: KeyboardEvent) => {
    if (event.code === 'Enter' && event.ctrlKey) {
      this.submitSubtitle()
      return false
    }
    
    if (event.code === 'Escape')  
      this.application.settings.switchEditing()

    if ((event.key === 'i' || event.key === 'b' || event.key === 'u') && event.ctrlKey) {
      const settings: TagSettings = { tag: event.key as TagEnum }
      this.addTags(settings)
    }

    if (event.key === 'f' && event.ctrlKey) {
      const settings: TagSettings = {
        tag: TagEnum.FONT,
        parameters: [ { name: 'color', value: '#ffffff' } ]
      }
      
      this.addTags(settings)
    }
  }

  changeCompletion = () => {
    if (this.wordCompletion) {
      this.wordCompletion = false
      this.suggestionContainer.nativeElement.children[0].innerText = ''
      this.notifier.addInfoNotification('Disabled the word completion, the suggestions will no longer be displayed')
    } else {
      this.wordCompletion = true
      this.notifier.addInfoNotification('Enabled the word completion, the suggestions will now be displayed')
    }

    if (this.application.hasConsent)
      localStorage.wordCompletion = this.wordCompletion
  }
  
  setSuggestion = () => {
    if (!this.wordCompletion || navigator.userAgent.match(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/))
      return

    const contentElement = this.content.nativeElement
    const suggestionElement = this.suggestionContainer.nativeElement.children[0]
    
    const content = contentElement.value
    const lines = content.split('\n')

    const word = lines.join(' ').split(' ').pop()
    const wordIndex = content.lastIndexOf(word)
    const prefix = content.substring(0, wordIndex)

    if (!word || word.length === 0) {
      suggestionElement.innerText = content
      this.onScroll()
      return
    }

    const suggestion = ContentManager.getSuggestion(word, this.application.newSrtFile.subtitles)
    const result = suggestion ? prefix + suggestion.split(/\W/)[0] : content

    suggestionElement.innerText = result
    this.onScroll()
  }

  submitSubtitle = () => {
    const content = this.content.nativeElement.value.trim()
    const endTime = this.endTime.getTime()
    const startTime = this.startTime.getTime()
    
    const subtitle = this.subtitle
    const subtitleBefore = subtitle.clone()
    
    if (endTime.getMilliseconds() < startTime.getMilliseconds()) {
      this.notifier.addErrorNotification('The subtitle\'s start time cannot be higher than it\'s end time')
      return
    }

    if (content.length === 0) {
      this.notifier.addErrorNotification('The subtitle\'s content cannot be empty')
      return
    }

    const isChanged = content.split('\r\n').length > 1
    let contentResult = isChanged ? content : ''

    this.application.settings.isDragging = false
    
    if (!isChanged) {
      const contentBreaks = content.split('\n').filter((c: string) => c.trim().length > 0)  
  
      for (const c of contentBreaks)
        contentResult += c + '\r\n'
    }

    subtitle.content = contentResult.trim()
    subtitle.startTime = startTime
    subtitle.endTime = endTime

    if (subtitle.internalID === undefined) {
      this.subtitleManager.addSubtitle(subtitle)
    } else {
      const change = new SubtitleChange(subtitleBefore, subtitle)
      this.changesManager.addChanges([ change ])
    }

    this.application.sortByTime()
    this.selection.manager.deleteFirst()

    if (this.selection.manager.selected.length === 0)
      this.application.settings.switchEditing()
    else
      this.subtitle = this.selection.manager.selected[0]
  }
}