import { Component } from '@angular/core'
import { TagSettings, TagManager, TagEnum } from 'src/app/classes/TagManager'
import { FileService } from 'src/app/services/FileService.service'
import { ChangesService } from 'src/app/services/ChangesService.service'
import { NotifierService } from 'src/app/services/NotifierService.service'
import { SelectionService } from 'src/app/services/SelectionService.service'
import { ApplicationService } from 'src/app/services/ApplicationService.service'
import { InputNotification } from 'src/app/classes/Notification'
import { SubtitleService } from 'src/app/services/SubtitleService.service';
import { SrtFile } from 'src/app/modules/SrtFile';

@Component({
  selector: 'actions',
  templateUrl: './Actions.component.html',
  styleUrls: [ './Actions.component.css' ]
})
export class ActionsComponent {
  TagEnum = TagEnum

  constructor (public application: ApplicationService, public selection: SelectionService, public changeManager: ChangesService,
               public fileService: FileService, private notifier: NotifierService, private subtitleManager: SubtitleService) { }

  public addFontTag () {
    const notification = new InputNotification('Enter the color you want the selected subtitles to be')

    notification.onSubmit = () => {
      const settings: TagSettings = { tag: TagEnum.FONT, parameters: [ { name: 'color', value: notification.inputValue } ] }
      this.addTags(settings)      
    }

    this.notifier.addNotification(notification)
  }

  public addTags (settings: TagSettings) {
    const selected = this.selection.manager.selected
    const subtitles = selected.length === 0 ? this.application.newSrtFile.subtitles : selected
    const changes = TagManager.changeTag(subtitles, settings)

    this.changeManager.addChanges(changes)
    this.notifier.addInfoNotification(`Applied tag change to ${changes.length} subtitle${changes.length !== 1 ? 's' : ''}`)
  }

  public async clipboardExec (type: string) {
    let result: string

    switch (type) {
      case 'copy': 
        result = this.subtitleManager.copySubtitles()
        navigator.clipboard.writeText(result)
        break
      case 'cut':
        result = this.subtitleManager.copySubtitles()
        navigator.clipboard.writeText(result)
        this.selection.removeSelected()
        break
      case 'paste':
        const clipboard = await navigator.clipboard.readText()

        if (typeof clipboard === 'string') {
          const srt = new SrtFile(clipboard.trim())
    
          this.subtitleManager.addSubtitles(srt.subtitles)
          this.selection.manager.selected = [ ]
        }

        break
    }
  }

  public createFile () {
    const file = new File([ ], 'new_srt_file.srt')
    this.fileService.loadFile(file)
  }

  public loadFile (event: Event) {
    const element = event.target as HTMLInputElement
    this.fileService.loadFile(element.files[0])
  }
}