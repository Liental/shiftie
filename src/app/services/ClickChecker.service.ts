import { DropdownComponent } from 'src/app/components/application/dropdowns/Dropdown/Dropdown.component'
import { SelectComponent } from 'src/app/components/application/dropdowns/Select/Select.component'

/** A service used to monitor mouse clicks on the site to determine if any dropdown-like element should be closed. */
export class ClickChecker {
  selectCheck: SelectComponent[] = []
  dropdownCheck: DropdownComponent[] = []

  /** 
   * Method used for mouse events, checks if the mouse clicked on any dropdown-like element
   * @param e - mouse event argument used to see the click target 
  */
  clickEvent = (e: Event) => {
    this.checkArray(this.selectCheck, 'select', e.target)
    this.checkArray(this.dropdownCheck, 'dropdown', e.target)
  }

  /**
   * Checks if position of any dropdown-like element matches the mouse event target, if it doesn't, the element is closed
   * @param elements - elements to match the mouse click target with
   * @param tag - tag of given element, it's used to get its native element
   * @param target - target of mouse event to match with given element 
  */
  checkArray = (elements: any[], tag: string, target: EventTarget) => {
    for (const elem of elements) {
      if (!elem.revealed)
        continue

      const element = elem[tag].nativeElement
      const isOutside = this.checkElement(element, target)

      if (!isOutside) 
        elem.revealed = false
    }
  }

  /** 
   * Recursively checks click target's parents to check if they match given element
   * @param nativeElement - native element of given dropdown-like element
   * @param clcikTarget - target element of triggered mouse click
  */
  checkElement = (nativeElement: Element, clickTarget: EventTarget) => {
    const clickElement = clickTarget as Element
    const clickParent = clickElement.parentElement

    if (nativeElement === clickTarget) 
      return true

    if (!clickParent || clickParent.tagName === 'body')
      return false
    
    return this.checkElement(nativeElement, clickParent)
  }
}
