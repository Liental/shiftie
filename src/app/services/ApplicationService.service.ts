import { Injectable } from '@angular/core'
import { SrtFile } from '../modules/SrtFile'
import { Subtitle } from '../modules/Subtitle'
import { ToolsSettings } from '../classes/ToolsSettings'

@Injectable()
export class ApplicationService {
  public limit = 40
  public position = 20
  public hasConsent = false
  public srtFile = new SrtFile()
  public newSrtFile = new SrtFile()
  public settings = new ToolsSettings()

  /** Returns the length of new SRT file subtitles */
  public getLength () {
    return this.newSrtFile.subtitles.length
  }

  /** Returns floored half of the limit */
  public getHalfLimit () {
    return Math.floor(this.limit / 2)
  }

  /** Sort all subtitles in new SRT file by their time and assign them IDs if the clean up is enabled */
  public sortByTime () {
    if (!this.settings.isCleaning)
      return

    const getTimes = (subtitle: Subtitle) => [ subtitle.startTime.getMilliseconds(), subtitle.endTime.getMilliseconds() ] 

    const subtitles = this.newSrtFile.subtitles.sort((a, b) => {
      const aTimes = getTimes(a) 
      const bTimes = getTimes(b)

      const isStartEarlier = aTimes[0] < bTimes[0]
      const isEndEarlier = aTimes[1] < bTimes[1] 

      if (isStartEarlier && isEndEarlier)
        return -1

      return isStartEarlier && aTimes[1] === bTimes[1] ? -1 : 1
    })

    for (let i = 0; i < subtitles.length; i++) 
      subtitles[i].id = i + 1

    this.newSrtFile.subtitles = subtitles
  }
}