import { Injectable } from '@angular/core'
import { Notifier } from '../modules/Notifier'
import { Notification, NotificationType } from '../classes/Notification'

@Injectable()
export class NotifierService {
  private manager = new Notifier()

  public getNotifications () {
    return this.manager.notifications
  }

  public closeNotification (notification: Notification) {
    this.manager.closeNotification(notification)
  }

  public addNotification (notification: Notification) {
    this.manager.addNotification(notification)
  }

  public addInfoNotification (content: string) {
    const notification  = new Notification(content, NotificationType.INFO, 2500)
    this.addNotification(notification)
  }

  public addWarningNotification (content: string) {
    const notification  = new Notification(content, NotificationType.WARNING, 2500)
    this.addNotification(notification)
  }

  public addErrorNotification (content: string) {
    const notification  = new Notification(content, NotificationType.ERROR, 3500)
    this.addNotification(notification)
  }
}