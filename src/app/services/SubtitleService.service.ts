import { Injectable } from '@angular/core'
import { Subtitle } from '../modules/Subtitle'
import { IDGenerator } from '../classes/IDGenerator'
import { ChangesService } from './ChangesService.service'
import { SubtitleChange } from '../classes/SubtitleChange'
import { NotifierService } from './NotifierService.service'
import { SelectionService } from './SelectionService.service'
import { ApplicationService } from './ApplicationService.service'

@Injectable()
export class SubtitleService {
  constructor (private application: ApplicationService, private changesManager: ChangesService, private notifier: NotifierService, 
               private selection: SelectionService) { }
  
  /** Add new subtitle and select it */
  public newSubtitle () {
    const subtitle = new Subtitle()
    this.selection.manager.selectSubtitle(subtitle)
  }

  /**
   * Add provided subtitle, if not provided, use first selected one
   * @param subtitle - subtitle to add to the file
  */
  public addSubtitle (subtitle: Subtitle) {
    subtitle.id = this.application.getLength() + 1
    this.addSubtitles([ subtitle ])
  }

  /**
   * Removes given subtitle
   * @param subtitle - subtitle to delete or its index
  */
  public removeSubtitle (subtitle: Subtitle | number) {
    const id = typeof subtitle === 'number' ? subtitle : subtitle.id - 1
    this.application.newSrtFile.subtitles.splice(id, 1)
  }

  /**
   * Add given subtitles to the file and push changes
   * @param subtitles - subtitles to add to the file 
  */
  public addSubtitles (subtitles: Subtitle[]) {
    const changes: SubtitleChange[] = [ ]

    for (const subtitle of subtitles) {
      subtitle.internalID = IDGenerator.generateID()
      changes.push(new SubtitleChange(undefined, subtitle))
    }

    if (changes.length > 0) {
      this.changesManager.addChanges(changes)
      this.application.newSrtFile.subtitles = subtitles.concat(this.application.newSrtFile.subtitles)
      
      this.application.sortByTime()
      this.notifier.addInfoNotification(`Successfully added ${changes.length} subtitle${changes.length !== 1 ? 's' : ''} to the file`)
    }
  }

  /** Display currently selected subtitles and return the result */
  public copySubtitles () {
    const subtitles = this.selection.manager.selected
    const result = subtitles.map((subtitle) => {
      const index = subtitles.indexOf(subtitle)
      const lineBreak = index < subtitles.length ? '\r\n\r\n' : ''
    
      return subtitle.displayRaw() + lineBreak
    })

    if (result.length > 0)
      this.notifier.addInfoNotification(`Successfully copied ${subtitles.length} subtitle${subtitles.length !== 1 ? 's' : ''}`)
    
    return result.join('')
  }

  /**
   * Choose a subtitle to shift / edit / delete
   * @param subtitle - subtitle to choose
  */
  public chooseSubtitle (subtitle: Subtitle) {
    const settings = this.application.settings

    if (!settings.isFromTo && !settings.isSelecting)
      this.application.settings.switchEditing()
    
    if (settings.isFromTo && this.selection.manager.lastSelected)
      this.selection.selectFromTo(subtitle)
    else
      this.selection.chooseSubtitle(subtitle)
  }
  
  /**
   * Replace a string of text in the selected sutitles with another one (replqce in all the subtitles if none are selected)
   * @param oldValue - a string of text to find and replace
   * @param newValue - a string of text to replace the old value with
   * @param isRegex = is given value a regex value
  */
  public replace (oldValue: string, newValue: string, isRegex: boolean = false) {
    if (oldValue === newValue || oldValue === '')
      return
      
    const changes: SubtitleChange[] = [] 
    const value = isRegex ? new RegExp(oldValue, 'g') : oldValue
    
    let subtitles = this.selection.manager.selected
 
    if (subtitles.length === 0)
      subtitles = this.application.newSrtFile.subtitles
      
    for (const subtitle of subtitles) {
      const before = subtitle.clone()
      const regexMatch = isRegex && subtitle.content.match(value)
      const stringMatch = typeof value === 'string' && subtitle.content.includes(value)

      if (!regexMatch && !stringMatch)
        continue

      if (isRegex)
        subtitle.content = subtitle.content.replace(value, newValue)
      else
        subtitle.content = subtitle.content.split(value).join(newValue)
      
      const change = new SubtitleChange(before, subtitle)
      changes.push(change)
    }

    if (changes.length > 0) {
      this.changesManager.addChanges(changes)
      this.notifier.addInfoNotification(`Replaced given strings in ${changes.length} subtitle${changes.length !== 1 ? 's' : ''}`)
    } else
      this.notifier.addInfoNotification('Given strings were not found in any subtitle')
  }
}