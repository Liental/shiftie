import { Injectable } from '@angular/core'
import { Filter } from '../modules/Filter'
import { Subtitle } from '../modules/Subtitle'
import { FilterService } from './FilterService.service'
import { ApplicationService } from './ApplicationService.service'

@Injectable()
export class CacheService {
  public filteredLength: number = 0

  private previousPosition: number = 0
  private newSubtitles: Subtitle[] = [ ]
  private originalSubtitles: Subtitle[] = [ ]
  private previousSubtitles: Subtitle[] = [ ]
  private previousFilter: Filter = new Filter()

  constructor (private application: ApplicationService, private filter: FilterService) { }

  /** Compares current application objects to previously cached ones to determine if they need rerendering  */
  public getCachedSubtitles () {
    const filterObject = this.filter.manager
    const srtFile = this.application.newSrtFile

    const filterEquals = filterObject.equals(this.previousFilter)
    const positionEquals = this.application.position === this.previousPosition
    const lengthEquals = srtFile.subtitles.length === this.previousSubtitles.length

    if (!lengthEquals || !filterEquals || !positionEquals || !this.equalSubtitles(this.previousSubtitles)) {
      const originalToCut = filterObject.filter(this.application.srtFile.subtitles)
      const newToCut = filterObject.filter(this.application.newSrtFile.subtitles)

      this.newSubtitles = this.cutSubtitles(newToCut)
      this.originalSubtitles = this.cutSubtitles(originalToCut)
      
      this.filteredLength = newToCut.length
      this.previousFilter = filterObject.clone()
      this.previousSubtitles = srtFile.cloneSubtitles()
      this.previousPosition = this.application.position
    }

    return [this.originalSubtitles, this.newSubtitles]
  }

  /** 
   * Cuts provided subtitles to fit the limit and help the performance and return the result 
   * @param subtitles - subtitles to cut  
  */
  public cutSubtitles (subtitles: Subtitle[]) {
    const half = this.application.getHalfLimit()
    const start = this.application.position - half
    const max = this.application.position + half
    const end = max > subtitles.length ? subtitles.length : max 

    return subtitles.slice(start, end)
  }

  /**
   * Compares given subtitles to the cut subtitles of new SRT file and equation result
   * @param subtitles - subtitles to compare
  */
  public equalSubtitles (subtitles: Subtitle[]) {
    const applicationSubtitles = this.application.newSrtFile.subtitles

    for (let i = 0; i < applicationSubtitles.length; i++) {
      const subtitle = subtitles[i]
      const isTheSame = subtitle && subtitle.equals(applicationSubtitles[i])
      
      if (!isTheSame)
        return false
    }

    return true
  }
}
