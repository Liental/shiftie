import { Injectable } from '@angular/core'
import { ChangesManager } from '../modules/ChangesManager'
import { SubtitleChange } from '../classes/SubtitleChange'
import { NotifierService } from './NotifierService.service'
import { ApplicationService } from './ApplicationService.service'

@Injectable()
export class ChangesService {
  public manager = new ChangesManager()

  constructor (private application: ApplicationService, private notifier: NotifierService) {
    
  }

  /** 
   * Add given changes to the changes manager
   * @param changes - changes to add 
   * @param revert - boolean indicating if given change is made or remade
  */
  public addChanges (changes: SubtitleChange[], revert?: boolean) {
    if (changes.length > 0)
      this.manager.addChanges(changes, revert)
  }
  
  /**
   * Remove and return last change from the changes manager and handle it properly
   * @param revert - boolean indicating if the change if from made or remade changes
  */
  public popChanges (revert: boolean = true) {
    const changes = this.manager.popChanges(revert)

    if (changes !== undefined) {
      this.handleChanges(changes, revert)
      this.notifier.addInfoNotification(`${revert ? 'Reverted' : 'Made'} a change`)
    }
  }

  /**
   * Add / remove / change subtitles based on provided subtitle changes
   * @param changes - changes to handle
   * @param revert - boolean indicating if the change was made or remade  
  */
  public handleChanges (changes: SubtitleChange[], revert: boolean = true) {
    for (const change of changes) {
      const subtitleFrom = revert ? change.subtitleFrom : change.subtitleTo
      const subtitleTo = revert ? change.subtitleTo : change.subtitleFrom
      
      // The change has no subtitleTo field which means the subtitle has been deleted
      if (!subtitleTo) {
        this.application.newSrtFile.subtitles.push(subtitleFrom.clone())
        continue
      }
      
      const subtitleIndex = this.application.newSrtFile.findSubtitleById(subtitleTo)

      // The change has no subtitleFrom field which means the subtitle has been added
      if (!subtitleFrom) {
        this.application.newSrtFile.subtitles.splice(subtitleIndex, 1)
        continue
      }

      // The change passed all the above conditions meaning the subtitle has been changed
      this.application.newSrtFile.subtitles[subtitleIndex] = subtitleFrom.clone()
    }

    this.application.sortByTime()
  }
}