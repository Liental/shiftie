import { Injectable } from '@angular/core'
import { Subtitle } from '../modules/Subtitle'
import { FilterService } from './FilterService.service'
import { NotifierService } from './NotifierService.service'
import { SelectionManager } from '../modules/SelectionManager'
import { ApplicationService } from './ApplicationService.service'
import { ChangesService } from './ChangesService.service'

@Injectable()
export class SelectionService {
  public manager = new SelectionManager()

  constructor (private application: ApplicationService, private filter: FilterService, private notifier: NotifierService,
               private changesManager: ChangesService) { }

  /** Returns the temporarly selected subtitle */
  public getTemporary () {
    return this.manager.temporarySelect
  }

  /**
   * Sets the temporary subtitle to the given value
   * @param subtitle - subtitle to set as temporary selection
  */
  public setTemporary (subtitle: Subtitle) {
    this.manager.temporarySelect = subtitle
  }

  /** Select all the subtitles from the application new srt file */
  public selectAll () {
    const subtitles = this.filter.filter(this.application.newSrtFile.subtitles)
    const length = subtitles.length

    if (length > 0) {
      this.manager.selectSubtitles(subtitles)
      this.notifier.addInfoNotification(`Selected ${length} subtitle${length !== 1 ? 's' : ''}`)
    }
  }
  
  /**
   * Choose a subtitle to shift / edit / delete
   * @param subtitle - subtitle to choose
  */
  public chooseSubtitle (subtitle: Subtitle) {
    const settings = this.application.settings

    if (!settings.isFromTo && !settings.isSelecting) {
      this.application.settings.switchEditing()
      this.application.settings.isEditing = true
    }
  
    if (settings.isFromTo && this.manager.lastSelected)
      this.selectFromTo(subtitle)
    else
      this.manager.chooseSubtitle(subtitle, settings.isSelecting)
  }

  /**
   * Select all subtitles that fit between the last selected subtitle and given subtitle
   * @param subtitle - subtitle being a limit to from-to selection 
  */
  public selectFromTo (subtitle: Subtitle) {
    const lastSelected = this.manager.lastSelected
    const subtitles = this.filter.filter(this.application.newSrtFile.subtitles)

    this.manager.selectFromTo(subtitles, subtitle, lastSelected)
  }

  /** Remove selected subtitles from the new srt file */
  public removeSelected () {
    const srtFile = this.application.newSrtFile
    const changes = this.manager.removeSelected(srtFile)

    if (changes.length === 0)
      return

    this.application.sortByTime()
    this.changesManager.addChanges(changes)
    this.application.settings.isFromTo = false
    this.application.settings.isSelecting = false
    this.notifier.addInfoNotification(`Successfully removed ${changes.length} subtitle${changes.length !== 1 ? 's' : ''}`)
  }
}