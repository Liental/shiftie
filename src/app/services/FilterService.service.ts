import { Injectable } from '@angular/core'
import { Filter } from '../modules/Filter'
import { Subtitle } from '../modules/Subtitle'

@Injectable()
export class FilterService {
  public manager = new Filter()

  /**
   * Looks for every subtitle in provided array that matches the filter and returns it
   * @param subtitles - array of subtitles to filter
  */
  public filter = (subtitles: Subtitle[]) => {
    return this.manager.filter(subtitles)
  }
}