import { Injectable } from '@angular/core'
import { SrtFile } from '../modules/SrtFile'
import { FileManager } from '../modules/FileManager'
import { NotifierService } from './NotifierService.service'
import { ChangesService } from './ChangesService.service'
import { FilterService } from './FilterService.service'
import { SelectionService } from './SelectionService.service'
import { ApplicationService } from './ApplicationService.service'
import { QuestionNotification, InputNotification } from '../classes/Notification'

@Injectable()
export class FileService {
  constructor (private application: ApplicationService, private notifier: NotifierService, private changes: ChangesService,
               private filter: FilterService, private selection: SelectionService) { }

  /** Reset everything this service contains (can be used to add new file) */
  public resetFiles () {
    this.filter.manager.reset()
    this.changes.manager.reset()
    this.selection.manager.reset()
    this.application.settings.reset()
    
    this.application.position = 20
    this.application.srtFile = new SrtFile()
    this.application.newSrtFile = new SrtFile()
  }

  /**
   * Checks the SRT files for changes and sets up notification with input if they are different otherwise reads the files
   * @param file - file to read and process
  */
  public loadFile (file: File) {
    const originalDisplay = this.application.srtFile.displayRaw()
    const newDisplay = this.application.newSrtFile.displayRaw()

    if (originalDisplay !== newDisplay) {
      const notification = new QuestionNotification('You have unsaved changes \nare you sure you want to quit this file?')
      notification.onSubmit = () => { this.readFile(file) }

      this.notifier.addNotification(notification)
      return
    }
    
    this.readFile(file)
  }

  /** 
   * Reads provided file and if the file is correct, reads its contents into SRT files 
   * @param file - file to read and process
  */
  public readFile (file: File) {
    FileManager.readFile(file)
    .then((res: SrtFile[]) => { 
      this.resetFiles()
      this.application.srtFile = res[0]
      this.application.newSrtFile = res[1]

      if (this.application.srtFile.subtitles.length > 0) {
        this.notifier.addInfoNotification('Successfully loaded a subtitle file')
      } else {
        this.application.settings.isDragging = false
        this.notifier.addInfoNotification('Successfully created a new subtitle file')
      }
    }).catch((message) => {
      this.notifier.addErrorNotification(message)
    })
  }

  /** Sets up notification with input to get a name for the file and then downloads it */
  public writeFile () {
    if (this.application.getLength() === 0) {
      this.notifier.addWarningNotification('The output is empty therefore there is nothing to save')
      return
    }

    const notification = new InputNotification('Enter a name for the file')
    notification.inputValue = this.application.newSrtFile.name

    notification.onSubmit = () => { 
      this.application.newSrtFile.name = notification.inputValue 
      FileManager.writeFile(this.application.newSrtFile) 
    }

    this.notifier.addNotification(notification)
  }
}