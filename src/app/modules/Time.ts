/**
 * Time class represents time with hours, minutes, seconds and milliseconds.
 * 
 * This class also contains a static definition of time units in milliseconds.
 * 
 * @param time - string in format of HH:MM:SS,mmm or number representing milliseconds that the object should be initialised from [Default 0]
*/
export class Time {
  hours: number = 0
  minutes: number = 0
  seconds: number = 0
  milliseconds: number = 0

  /** A table containing definition of time units in milliseconds */
  public static timeDefinition = {
    hours: 1000 * 60 * 60,
    minutes: 1000 * 60,
    seconds: 1000,
    milliseconds: 1
  }

  constructor (time: string | number = 0) {
    if (typeof time === 'string') {
      const timeSplit = time.split(':')
      const secondsSplit = timeSplit[2].split(',')

      this.hours = timeSplit[0].length > 0 ? parseInt(timeSplit[0]) : 0
      this.minutes = timeSplit[1].length > 0 ? parseInt(timeSplit[1]) : 0
      this.seconds = secondsSplit[0].length > 0 ? parseInt(secondsSplit[0]) : 0
      this.milliseconds = secondsSplit[1].length > 0 ? parseInt(secondsSplit[1]) : 0

      return
    }

    if (time <= 0)
      return

    for (const unit in Time.timeDefinition) {
      this[unit] = Math.floor(time / Time.timeDefinition[unit])
      time -= Time.timeDefinition[unit] * this[unit]
    }
  }

  /** Returns time associated with this object represented in milliseconds */
  getMilliseconds = () => {
    let millisecondsTime = 0

    for (const unit in Time.timeDefinition) 
      millisecondsTime += Time.timeDefinition[unit] * this[unit]

    return millisecondsTime
  }

  /** Deep clones this object and returns the clone */
  clone = () => {
    const display = this.display()
    const time = new Time(display)

    return time
  }

  /**
   * Compares this time with given one and returns the result of equation
   * @param time - time to compare with as Time object, nubmer or string
  */
  equals = (time: Time | number | string) => {
    const strThis = time instanceof Time || typeof time === 'string' ? this.display() : this.getMilliseconds()
    const strTime = time instanceof Time ? time.display() : time

    return strThis === strTime
  }

  /**
   * Shifts time associated with this object by provided time
   * @param time - time to shift represented by Time object or number in milliseconds
   * @param backwards - defined if the time is supposed to go back  
  */
  shift = (time: Time | number | string, backwards?: boolean) => {
    time = time instanceof Time ? time : new Time(time)

    const milliseconds = time.getMilliseconds()
    const toShift = backwards ? -milliseconds : milliseconds
    const newTime = new Time(this.getMilliseconds() + toShift)

    for (const unit in Time.timeDefinition) 
      this[unit] = newTime[unit]
  }

  /** Returns time associated with this object in HH:MM:SS,mmm format */
  display = () => {
    const fillZeros = (count: number, text: number) => {
      const isNegative = text.toString().length > count
      const repeatCount = isNegative ? 0 : count - text.toString().length

      return '0'.repeat(repeatCount) + text
    }

    const time = [
      fillZeros(2, this.hours),
      fillZeros(2, this.minutes),
      fillZeros(2, this.seconds),
      fillZeros(3, this.milliseconds)
    ]

    return `${time[0]}:${time[1]}:${time[2]},${time[3]}`
  }
}
