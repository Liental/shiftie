import { Notification } from '../classes/Notification'

/** Class containing notification system and management */
export class Notifier {
  public limit: number = 5
  public queue: Notification[] = []
  public notifications: Notification[] = []

  /**
   * Removes given notification from the table and adds first notification from queue if there is one
   * @param notification - notification to remove from the table
  */
  public closeNotification (notification: Notification) {
    const index = this.notifications.indexOf(notification)
    this.notifications.splice(index, 1)

    if (this.queue.length > 0)
      this.addNotification(this.queue.shift())
  }

  /**
   * Adds given notification to the table if there is space, if not adds it to the queue instead
   * @param notification - notification to add to the table
  */
  public addNotification (notification: Notification) {
    if (this.notifications.length >= this.limit) {
      this.queue.push(notification)
      return
    }
    
    this.notifications.push(notification)
    if (notification.duration && !isNaN(notification.duration)) 
      return setTimeout(() => this.closeNotification(notification), notification.duration)
  }
}