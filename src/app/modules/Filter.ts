import { Time } from './Time'
import { Subtitle } from './Subtitle'

/** Class used to filter and sort subtitles with given options */
export class Filter {
  /** Field used to filter by given ID */
  id: number

  /** Field used to match the start time of a subtitle */
  startTime: Time

  /** Field used to match the end time of a subtitle */
  endTime: Time

  /** Field used to filter subtitles by their content, checks if a subtitle contains this field */
  content: string

  /** Field used to determine if the sort should be ascending [1] or descending [-1] */
  orderBy: number = 1

  /** 
   * Field used to determine how should the subtitles be sorted
   * @param subtitle - subtitles to sort 
  */
  sortClosure = (subtitle: Subtitle) => subtitle.id

  /** Deeply clones the filter and return the clone */
  clone = () => {
    const filter = new Filter()
    const variables = [ 'id', 'startTime', 'endTime', 'content', 'orderBy' ]

    for (const variable of variables) 
      filter[variable] = this[variable]

    return filter
  }

  /** Checks if the filter does not have any requirements */
  isEmpty = () => {
    const firstPart = this.id === undefined && this.content === undefined
    const secondPart = this.startTime === undefined && this.endTime === undefined

    return firstPart && secondPart
  }

  /** 
   * Checks if given filter is the same as this filter
   * @param filter - filter to check
  */
  equals = (filter: Filter) => {
    const stringSelf = JSON.stringify(this)
    const stringFilter = filter ? JSON.stringify(filter) : ''

    return stringSelf === stringFilter
  }

  /** Restarts this filter to its initial values */
  reset = () => {
    this.id = undefined
    this.startTime = undefined
    this.endTime = undefined
    this.content = undefined
    this.orderBy = 1
    this.sortClosure = (s: Subtitle) => s.id
  }

  /** 
   * Sorts given subtitles using sortClosure and returns them
   * @param subtitles - subtitles to sort
  */
  sort = (subtitles: Subtitle[]) => {
    subtitles = subtitles.sort((a: Subtitle, b: Subtitle) => {
      const aValue = this.sortClosure(a)
      const bValue = this.sortClosure(b)

      return aValue > bValue ? this.orderBy : -this.orderBy
    })
    
    return subtitles
  }

  /**
   * Checks if given times are the same and returns result of equation 
   * (if a field equals -1 it's ignoring the field)
   * @param aTime - time to compare with bTime
   * @param bTime - time to compare with aTime
  */
  checkTime = (aTime: Time, bTime: Time) => {
    for (const unit in Time.timeDefinition) {
      if (aTime[unit] !== bTime[unit] && bTime[unit] !== -1)
        return false
    } 
      
    return true
  }

  /**
   * Filters given subtitles with options defined before and returns them sorted
   * @param subtitles - subtitles to filter and sort
  */
  filter = (subtitles: Subtitle[]) => {
    const searchTerm = this.content ? this.content : ''

    const result = subtitles.filter((s) => {
      const isTerm = s.content.match(searchTerm)
      const isID = s.id === this.id || !this.id

      const isTimeStart = !this.startTime || this.checkTime(s.startTime, this.startTime)
      const isTimeEnd = !this.endTime || this.checkTime(s.endTime, this.endTime)
      const isTime = isTimeStart && isTimeEnd

      return isTerm && isID && isTime
    })

    return this.sort(result)
  }
}
