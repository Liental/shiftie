import { SubtitleChange } from '../classes/SubtitleChange'

/** Class managing and handling all registered changes in the files */
export class ChangesManager {
  /** Changes that were made and not reverted */
  madeChanges: SubtitleChange[][] = []

  /** Changes that were reverted */
  revertedChanges: SubtitleChange[][] = []

  /** Clears both changes arrays */
  reset = () => {
    this.madeChanges = []
    this.revertedChanges = []
  }

  /** 
   * Compares given changes to the latest element of local changes list and returns the result of equation 
   * @param changes - changes to compare with local changes
   * @param revert - defines if given changes are meant to be compared with made or reverted changes  
 */
  compareChanges = (changes: SubtitleChange[], revert?: boolean) => {
    const takeArray = revert ? this.revertedChanges : this.madeChanges
    const latest = takeArray[takeArray.length - 1]

    if (!latest || latest.length !== changes.length)
      return false

    for (let i = 0; i < changes.length; i++) {
      if (changes[i].id !== latest[i].id)
        return false
    }

    return true
  }

  /**
   * Adds given change to array specified by undo boolean
   * @param changes - changes to push inside local changes array
   * @param revert - boolean defining if the changes should be pushed into made or reverted changes array
  */
  addChanges = (changes: SubtitleChange[], revert?: boolean) => {
    const pushArray = revert ? this.revertedChanges : this.madeChanges
    const shouldPush = !this.compareChanges(changes, revert)

    if (!revert && shouldPush) 
      this.revertedChanges = []

    if (shouldPush)
      pushArray.push(changes)
  }

  /**
   * Removes and returns latest changes from changes array
   * @param revert - defines if the changes are supposed to be pulled from made or unamde changes array [Default true]
  */
  popChanges = (revert: boolean = true) => {
    const takeArray = revert ? this.madeChanges : this.revertedChanges
    const pushArray = revert ? this.revertedChanges : this.madeChanges
    const changes = takeArray.pop()
    
    if (changes)
      pushArray.push(changes)

    return changes
  }
}