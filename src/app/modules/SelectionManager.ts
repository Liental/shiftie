import { Subtitle } from './Subtitle'
import { SrtFile } from './SrtFile'
import { SubtitleChange } from '../classes/SubtitleChange';

/** Class managing selection modes and selected subtitles */
export class SelectionManager {
  /** Subtitle used to determine which subtitle should from-to mode start from */
  lastSelected: Subtitle

  /** Subtitle selected using arrow keys */
  temporarySelect: Subtitle

  /** Subtitles selected by the user */
  selected: Subtitle[] = []

  /** Checks if provided subtitle is in the selection */
  isSelected = (subtitle: Subtitle) => {
    return this.findSubtitle(subtitle) !== -1
  }

  /** Resets this object to default value */
  reset = () => {
    this.selected = []
    this.lastSelected = undefined
    this.temporarySelect = undefined
  }

  /** Deletes the first element from the selection array */
  deleteFirst = () => {
    const subtitles = Array.from(this.selected)
    this.selected = subtitles.slice(1, subtitles.length)
  }

  /** 
   * Clear the seleciton array and add provided subtitle to it 
   * @param subtitle - subtitle to put in new array 
  */
  selectSubtitle = (subtitle: Subtitle) => {
    if (!subtitle)
      return

    this.temporarySelect = subtitle
    this.selected = [ subtitle ]
  }

  /**
   * Add all given subtitles to the selection
   * @param subtitles - subtitles to be selected
  */
  selectSubtitles = (subtitles: Subtitle[]) => {
    this.selected = Array.from(subtitles)
  }

  /**
   * Select all subtitles from the array that fit in the given range
   * @param subtitles - subtitles to take selection from
   * @param subtitleFrom - subtitle from which the seleciton is supposed to start
   * @param subtitleTo - subtitle which should define the end of selection
  */
  selectFromTo = (subtitles: Subtitle[], subtitleFrom: Subtitle, subtitleTo: Subtitle) => {
    const sorted = [ subtitleFrom, subtitleTo ].sort((a, b) => a.id > b.id ? 1 : -1)

    for (const subtitle of subtitles) {
      if (subtitle.id > sorted[1].id)
        break

      if (subtitle.id >= sorted[0].id && !this.isSelected(subtitle))
        this.changeSelection(subtitle)
    }
  }

  /** 
   * Find the index of provided subtitle in the array 
   * @param subtitle - subtitle to find index of
  */
  findSubtitle = (subtitle: Subtitle) => {
    return this.selected.indexOf(subtitle)
  }

  /** 
   * Choose temporary selected subtitle to join selection
   * @param selecting - defines if the subtitle should be chosen or added to selection
  */
  chooseTemporary = (selecting: boolean = false) => {
    this.chooseSubtitle(this.temporarySelect, selecting)
  }

  /**
   * Choose provided subtitle or add it to selection if selecting
   * @param subtitle - subtitle to choose
   * @param selecting - defines if the subtitle should be chosen or added to selection
  */
  chooseSubtitle = (subtitle: Subtitle, selecting: boolean = false) => {
    this.lastSelected = subtitle
    
    if (selecting)
      this.changeSelection(subtitle)
    else
      this.selectSubtitle(subtitle)
  }

  /** 
   * Add provided subtitle to selection or deselect it if it's already selected
   * @param subtitle - subtitle to add / remove from selection
  */
  changeSelection = (subtitle: Subtitle) => {
    const index = this.findSubtitle(subtitle)

    if (index === -1) {
      this.selected.push(subtitle)    
      this.selected.sort((a, b) => a.id > b.id ? 1 : -1)
      return
    }
    
    this.selected.splice(index, 1)
  }

  /** 
   * Removes all selected subtitles from provided SRT file 
   * @param srt - SRT file used to remove subtitles from 
  */
  removeSelected = (srt: SrtFile) => {
    const changes: SubtitleChange[] = this.selected.map((subtitle) => {
      const index = srt.findSubtitle(subtitle)
      srt.subtitles.splice(index, 1)

      return new SubtitleChange(subtitle.clone(), undefined)
    })

    this.reset()
    return changes
  }
}