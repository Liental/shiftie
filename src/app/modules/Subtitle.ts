import { Time } from './Time'
import { ShiftSettings } from '../classes/ShiftSettings'

/**
 * Class containing subtitle related methods and variables, used to display and manage subtitles
 * @param text - definition of a subtitle in SRT subtitle format
*/
export class Subtitle {
  /** Changable public ID of the subtitle to display */
  id: number

  /** End time of the subtitle */
  endTime: Time

  /** Start time of the subtitle */
  startTime: Time
  
  /** Content of the string */
  content: string
  
  /** Constant internal ID that represents the subtitle  */
  internalID: string
  
  constructor (text?: string) {
    if (!text) {
      this.content = ''
      this.startTime = new Time()
      this.endTime = new Time()

      return
    }

    text = text.split('<br>').join('\r\n')

    const textSplit = text.split('\r\n')
    const timeSplit = textSplit[1].split(' --> ')

    this.id = parseInt(textSplit[0])
    this.content = textSplit.slice(2).join('\r\n')

    this.startTime = new Time(timeSplit[0])
    this.endTime = new Time(timeSplit[1])
  }

  /**
   * Shifts start and end by given amount of time
   * @param time - time object or number representing milliseconds to shift
   * @param settings - an instance of ShiftSettings class to determine how to proceed with shifting 
  */
  shift = (time: Time | number | string, settings?: ShiftSettings) => {
    let result = false

    if (!settings)
      settings = new ShiftSettings()

    if (settings.shiftEnd) {
      result = true
      this.endTime.shift(time, settings.backwards)
    }

    if (settings.shiftStart) {
      const clonedTime = this.startTime.clone()
      clonedTime.shift(time, settings.backwards)

      if (clonedTime.getMilliseconds() <= this.endTime.getMilliseconds()) {
        result = true
        this.startTime = clonedTime
      }
    }

    return result
  }

  /** Clones this subtitle and returns its copy */
  clone = () => {
    const subtitle = new Subtitle()

    subtitle.id = this.id
    subtitle.internalID = this.internalID
    subtitle.content = this.content.slice()
    subtitle.startTime = this.startTime.clone()
    subtitle.endTime = this.endTime.clone()

    return subtitle
  }

  /**
   * Compares this subtitle to given one and returns the result of equation
   * @param subtitle - subtitle to compare with
  */
  equals = (subtitle: Subtitle) => {
    const isID = subtitle.id === this.id
    const isContent = subtitle.content === this.content
    const isTimeStart = subtitle.startTime.getMilliseconds() === this.startTime.getMilliseconds()
    const isTimeEnd = subtitle.endTime.getMilliseconds() === this.endTime.getMilliseconds()

    return isID && isTimeStart && isTimeEnd && isContent
  }

  /** Returns the duration of a subtitle in milliseconds */
  getDuration = () => {
    const startMs = this.startTime.getMilliseconds()
    const endMs = this.endTime.getMilliseconds()
  
    return endMs - startMs
  }

  /** Returns pretty print of the subtitle in SRT format */
  displayRaw = () => {
    const time = `${this.startTime.display()} --> ${this.endTime.display()}`
    const result = [ this.id, time, this.content ].join('\r\n')

    return result
  }

  /** Returns pretty print of the subtitle in SRT format with <br> instead of new lines */
  displayHTML = () => {
    const time = `${this.startTime.display()} --> ${this.endTime.display()}`
    const result = [ this.id, time, this.content ].join('<br>')

    return result
  }
}
