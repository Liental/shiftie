import { Time } from './Time'
import { Subtitle } from './Subtitle'
import { IDGenerator } from '../classes/IDGenerator'

/**
 * Class containing SRT contents and methods related to them
 * @param text - text representation of subtitles in SRT format
*/
export class SrtFile {
  /** Name of the SRT file */
  public name: string

  /** List of subtitles in the file */
  public subtitles: Subtitle[] = []

  constructor (text: string = '') {
    let sections = text.split('\r\n\r\n')

    if (sections.length === 1) {
      text = text.split('\n').join('\r\n')
      sections = text.split('\r\n\r\n')
    } 

    for (const section of sections) {
      if (section.trim().length === 0) 
        continue

      const isLinebreak = section.trim().split('\r\n').length >= 3
      const isBreakTag = section.split('<br>').length >= 3

      if (!isLinebreak && !isBreakTag)
        continue

      const sub = new Subtitle(section)
      sub.internalID = IDGenerator.generateID()
      
      this.subtitles.push(sub)
    }
  }

  /**
   * Find index of a subtitle and return
   * @param subtitle - subtitle to look up in the list
  */
  public findSubtitle (subtitle: Subtitle) {
    return this.subtitles.indexOf(subtitle)
  }

  /**
   * Find index of a subtitle with given internal ID
   * @param subtitle - subtitle to take the ID from or its ID
  */
  public findSubtitleById (subtitle: Subtitle | string) {
    const subtitleID = subtitle instanceof Subtitle ? subtitle.internalID : subtitle

    for (const sub of this.subtitles) {
      if (sub.internalID === subtitleID)
        return this.subtitles.indexOf(sub)
    }

    return -1
  }

  /**
   * Clones all given subtitles and returns their owned copy
   * @param subtitles - subtitles to clone [Default subtitles of this object]
  */
  public cloneSubtitles (subtitles: Subtitle[] = this.subtitles) {
    const ownedSubtitles: Subtitle[] = []

    for (const sub of subtitles) 
      ownedSubtitles.push(sub.clone())

    return ownedSubtitles
  }

  /** Pretty prints this object in SRT format and returns it */
  public displayRaw () {
    let result = ''

    for (const sub of this.subtitles) 
      result += sub.displayRaw() + '\r\n\r\n'

    return result
  }

  /** Pretty prints this object in SRT format and returns it in HTML */
  public displayHTML () {
    const result = this.displayRaw()
    return result.split('\r\n').join('<br>').trim()
  }
}
