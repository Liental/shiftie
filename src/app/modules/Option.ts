/** Class used to display an option of select dropdown */
export class Option {
  /** Value of the option */
  value: any
  
  /** Display value of the option */
  display: string
}
