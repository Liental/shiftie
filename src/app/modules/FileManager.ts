import { SrtFile } from './SrtFile'
import { saveAs } from 'file-saver'

/** Class containing static methods to write and read files */
export class FileManager {
  /**
   * Writes given SRT into a file using the name field in given object
   * @param srt - SRT object to write into a file 
  */
  public static writeFile = (srt: SrtFile) => {
    if (!srt.name)
      srt.name = 'new_srt_file'
    
    const file = new Blob([ srt.displayRaw() ])
    const extension = srt.name.includes('.srt') ? '' : '.srt'

    saveAs(file, srt.name + extension)
  }

  /**
   * Reads gives file and returns parsed results in an array 
   * @param file - file to read and parse 
  */
  public static readFile = (file: File) => {
    return new Promise((resolve, reject) => {
      if (!file)
        return reject('There is no file to be read')
    
      const reader = new FileReader()
      reader.onload = () => {
        const result = reader.result.toString()

        let srtFile: SrtFile
        let newSrtFile: SrtFile

        try {
          srtFile = new SrtFile(result)
          newSrtFile = new SrtFile()
        } catch (e) {
          return reject('The provided file is invalid')
        }
        
        newSrtFile.subtitles = srtFile.cloneSubtitles()
        
        srtFile.name = file.name
        newSrtFile.name = file.name

        resolve([ srtFile, newSrtFile ])
      }

      reader.readAsText(file)
    })
  }
}