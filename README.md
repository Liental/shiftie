[![pipeline status][gitlab-build]][gitlab]
[![coverage report][gitlab-coverage]][gitlab]

# Shiftie
Shiftie is an application which allows you to create, edit, and shift the time of subtitles in SRT files.  
You can use it [here][shiftie]!

## Quick-Start
The main model which is placed on the very top is used to upload files and to precise the time to shift.

### Optional
After uploading a SRT file, you can click the `→` button to load the subtitles.  
You can also use the `<` and `>` buttons to shift time backwards or forwards.

The upper part of the container contains tools to allows you to create, delete, and manipulate subtitles.

On the right side of the tools section are filters; a simple UI that allows you to sort and filter subtitles.

## Shortcuts
Throughout the application you can use keyboard shortcuts to quickly navigate between the subtitles and edit them.

| Shortcut         | Purpose          |
|------------------|------------------| 
| `h`              | Replace          |
| `c`              | Clear Filter     |
| `s`              | Open Sorting     |
| `f`              | Open Filtering   |
| `t`              | Open Time Model  |
| `shift`          | Change Selection |
| `space`          | Select           |
| `r / del`        | Delete           |
| `ctrl + a`       | Select All       |
| `ctrl + =`       | New Subtitle     |
| `ctrl + c`       | Copy             |
| `ctrl + x`       | Cut              |
| `ctrl + v`       | Paste            |
| `ctrl + s`       | Save             |
| `ctrl + z`       | Undo             |
| `ctrl + y`       | Redo             |
| `ctrl + alt + z` | Redo             |

[gitlab]: https://gitlab.com/Liental/shiftie/commits/master "GitLab Repository"
[gitlab-build]: https://gitlab.com/Liental/shiftie/badges/master/pipeline.svg "GitLab Pipeline"
[gitlab-coverage]: https://gitlab.com/Liental/shiftie/badges/master/coverage.svg "GitLab Code Coverage"
[shiftie]: https://shiftie.liental.space/ "Shiftie Homepage"
